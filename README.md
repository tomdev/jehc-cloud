# jehc

 **申明
功能迭代中...** 
### 微服务版本
集成产品线版本：https://gitee.com/jehc/jehc-cloud.git

工作流版本：https://gitee.com/jehc/jehc-cloud-workflow

IOT版本：https://gitee.com/jehc/jehc-cloud-iot

报表版本：https://gitee.com/jehc/jehc-cloud-report

病历云版本：https://gitee.com/jehc/jehc-cloud-medical

即时通讯版本：https://gitee.com/jehc/jehc-cloud-im

代码生成器：https://gitee.com/jehc/jehc-cloud-help


### 单工程2.0版本
纯净版本：https://gitee.com/jehc/jehc

工作流版本：https://gitee.com/jehc/jehc-workflow

IOT版本：https://gitee.com/jehc/jehc-iot


### 单工程1.0版本
纯净版本：https://gitee.com/jehc/jehc-boot

#### 介绍
JEHC-CLOUD基于Spring Cloud 2.X版本，采用前后端分离

集成了多方向平台如互联网，物联网，传统软件，医疗方向等

 **技术栈** 

 **后端** 

Spring，
SpringBoot2.0，
Mybatis，
PageHelper，
Solr全文检索，
Redis，
Ehcache，
JWT，
Oauth2，
数据库读写分离，
Activity5.21工作流，
客户端负载均衡Rule，
Sentinel限流体系，
Nacos注册中心 配置中心，
Gateway网关，
Junit，
Netty，
Quartz调度器，
FTP，
ES全文检索



 **前端** 

可视化流程设计器，
Bootstrap4.0，
Jquery2，
DataTables，
Mxgraph，
PDFJS，
ZTree

 **开发工具** 

  eclipse-jee-mars-1、eclipse-jee-mars-2、eclipse-juno、STS、IDEA

#### 软件架构
 **架构图** 
![输入图片说明](https://gitee.com/uploads/images/2019/0424/112257_71e2da4a_1341290.png "微服务架构图.png")
 **后端工程** 
![输入图片说明](screenshot/%E5%B7%A5%E7%A8%8B%E5%9B%BE.png)

 **前端工程** 

![输入图片说明](screenshot/%E5%89%8D%E7%AB%AF%E9%A1%B5%E9%9D%A2.png)
 **授权中心数据结构**
![输入图片说明](https://images.gitee.com/uploads/images/2021/0313/221812_6cc2e51a_1341290.png "JEHC-CLOUD微服务授权中心数据结构.png") 

#### 安装教程

1. 安装mysql5.7++数据库（其它数据库如Oracle）
2. 安装IntelliJ IDEA 2017.3.2 x64开发工具
3. 安装apache-maven-3.2.1及本地库repository
4. 安装Redis3版本以上
5. 导入JEHC-CLOUD工程项目
6. 设置maven环境

#### 功能模块
**工作流**
![输入图片说明](screenshot/%E6%B5%81%E7%A8%8B%E7%AE%A1%E7%90%86.jpg)
![输入图片说明](screenshot/%E5%9C%A8%E7%BA%BF%E8%AE%BE%E8%AE%A1.jpg)
![输入图片说明](screenshot/%E6%B4%BB%E5%8A%A8%E4%BB%BB%E5%8A%A1.jpg)
![输入图片说明](screenshot/%E6%B5%81%E7%A8%8B%E6%8A%A5%E8%A1%A8.jpg)
![输入图片说明](screenshot/%E6%B5%81%E7%A8%8B%E5%9B%BE.jpg)


**IOT**
![输入图片说明](screenshot/IOT.jpg)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/111959_ba6cc0b7_1341290.png "tpt.png")

**运管**
![输入图片说明](screenshot/%E7%89%A9%E7%90%86%E6%9C%BA%E7%9B%91%E6%8E%A7.jpg)
![输入图片说明](screenshot/Redis%E7%9B%91%E6%8E%A7.png)

### QQ群：673790569
### 邮箱：hxtkdcj@163.com