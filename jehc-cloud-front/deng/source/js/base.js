//////////////服务端API相关API///////////////

//网关集成基础地址API（总控API）
var baseApi = "http://127.0.0.1:9527";

//授权中心API
var oauthModules = baseApi + "/oauth"

//监控中心API
var monitorModules = baseApi + "/iotMonitor";

//运维中心API
var opModules = baseApi + "/iotOp";

//平台中心API
var sysModules = baseApi+ "/sys";

//工作流中心API
var workflowModules = baseApi+ "/workflow";

//文件中心API
var fileModules = baseApi+ "/file";

//日志中心API
var logModules = baseApi+ "/log";

//即时通讯中心API
var imModules = baseApi+ "/im";

//即时通讯中心WebSocket
var imWebSocketModules = baseApi+ "/imWebSocket";

//报表中心API
var reportModules = baseApi+ "/report";

//调度中心API
var jobModules = baseApi+ "/job";

//运管中心API
var ompModules = baseApi+ "/omp";

//病历云API
var medicalModules = baseApi+ "/medical";

//电商会员API
var memberModules = baseApi+ "/member";

//电商商户API
var sellerModules = baseApi+ "/seller";

//电商商品API
var productModules = baseApi+ "/product";

//电商订单API
var orderModules = baseApi+ "/order";

//电商支付API
var payModules = baseApi+ "/pay";

//////////////HTML相关基础路径///////////////
var basePath = "http://localhost:8081";

var loginPath = "http://localhost:8081/login.html";

var base_html_redirect = "http://localhost:8081/view";
