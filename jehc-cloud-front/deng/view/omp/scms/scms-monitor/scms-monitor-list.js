var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,ompModules+'/scms/monitor/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id"
            },
            {
                data:"id",
                render:function(data, type, row, meta) {
                    var host  = row.ip;
                    var mac = row.mac;
                    var btn = '<button onclick=toSCMSMonitorDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情">' +
                        '<i class="fa flaticon-eye"></i>' +
                        '</button>'
                    return btn+'<button onclick=initSCMSMonitorDetail("'+mac+'","'+host+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="监控信息">' +
                        '<i class="fa flaticon-graph"></i>' +
                        '</button>';
                }
            },
            {
                data:'user_name'
            },
            {
                data:'account_name'
            },
            {
                data:'com_name'
            },
            {
                data:'operate_sys_name'
            },
            {
                data:'operate_org'
            },
            {
                data:'ip'
            },
            {
                data:'mac'
            },
            {
                data:'mac',
                render:function(data, type, row, meta) {
                    var jvm_total_mem = row.jvm_total_mem;
                    var jvm_mem =  row.jvm_mem;
                    var jvm_cpu_count = row.jvm_cpu_count;
                    var environment = row.environment;
                    var path = row.path;
                    var mark = "JVM可使用总内存："+jvm_total_mem+"<br>";
                    mark+= "JVM可使用剩余内存："+jvm_mem+"<br>";
                    mark+= "JVM可使用处理器个数："+jvm_cpu_count+"<br>";
                    mark+= "运行环境版本："+environment+"<br>";
                    mark+= "安装path："+path+"<br>";
                    return mark;
                }
            },
            {
                data:"create_time",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"update_time",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            }
        ],
        fixedColumns:{
            leftColumns:7
        }
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
    //定时拉表格数据
    setInterval(function (args) {
        search('datatables');
    }, 50000);
});

//详情
function toSCMSMonitorDetail(id){

}

//删除
function delSCMSMonitor(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {id:id, _method:'DELETE'};
        ajaxBReq(ompModules+'/scms/monitor/delete',params,['datatables'],null,"DELETE");
    })
}

/**
 *
 * @type {string}
 */
var mac = "";
//服务器信息监控
function initSCMSMonitorDetail(data,host){
    $('#SCMSMonitorBody').height(reGetBodyHeight()-128);
    var SCMSMonitorModalCount = 0;
    $('#SCMSMonitorModal').modal({backdrop: 'static', keyboard: false});
    $("#SCMSMonitorModalLabel").html("实时信息 "+host+"");
    $('#SCMSMonitorModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#SCMSMonitorModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        mac = data;
        if(++SCMSMonitorModalCount == 1){
            initSCMSMonitorData();
            initAll();
        }
    });
}

/**
 *
 */
function initAll() {
    initSCMSMonitorMemChart();
    initSCMSMonitorMemData();
    initSCMSMonitorCpuChart();
    initSCMSMonitorCpuData();
}

/**
 *
 */
function initSCMSMonitorData() {
    if(mac == null || mac == ""){
        return;
    }
    $.ajax({
        url:ompModules+'/scms/monitor/mac/'+mac,
        type:"GET",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        // data:{mac:mac},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            $("#mac").html(result.data.mac);
                            $("#ip").html(result.data.ip);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
    initSCMSMonitorMemData();
}

/**
 * 定时任务
 */
var timer_ = setInterval(initAll,5000);


var memchart = null;
function initSCMSMonitorMemChart() {
    memchart = echarts.init(document.getElementById('mainMem'));
    memchart.setOption({
        title: {
            show:true,    // 是否显示标题组件,（true/false）
            text:'Memory',   // 主标题文本，支持使用\n换行
            textAlign:'auto',    //整体水平对齐（包括text和subtext）
            textVerticalAlign:'auto',//整体的垂直对齐（包括text和subtext）
            padding:0,    // 标题内边距 写法如[5,10]||[ 5,6, 7, 8] ,
            left:'auto',    // title组件离容器左侧距离，写法如'5'||'5%'
            right:'auto',    //'title组件离容器右侧距离
            top:'auto',    // title组件离容器上侧距离
            bottom:'auto',    // title组件离容器下侧距离
            borderColor: '#fff',     // 标题边框颜色
            borderWidth: 1,    // 边框宽度（默认单位px）
            textStyle: {    // 标题样式
                color: '#ccc',    //字体颜色
                fontStyle: 'normal',    //字体风格
                fontSize: 13,    //字体大小
                fontWeight: 400,    //字体粗细
                fontFamily: 'sans-serif',    //文字字体
                lineHeight: '13',    //字体行高
                align:'center',//文字水平对齐方式（left/right）
                verticalAlign:'middle',//文字垂直对齐方式（top/bottom）
            },
            subtext: '',    // 副标题
            subtextStyle: {    // 副标题样式
                color: '#ccc',
                fontStyle:'normal',
                fontWeight:'normal',
                fontFamily:'sans-serif',
                fontSize:11,
                lineHeight:11,
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        legend: {
            data: ['使用量', '剩余量', '交换区使用量', '交换区剩余量']
        },
        /*toolbox: {
            feature: {
                saveAsImage: {}
            }
        },*/
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: []
            }
        ],
        yAxis: [
            {
                // name: '单位（MB）',
                type: 'value',
                axisLabel:{
                    color: "#1e1e1e",
                    formatter: labelformatter,
                    inside: false,
                    fontSize:'0.2rem',
                    fontWeight: 'bold'
                }
            }
        ],
        series: [
            {
                name: '使用量',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#e7bcf3' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#e7bcf3' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: '剩余量',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: 'rgba(58,132,255, 0.5)' // 0% 处的颜色
                        }, {
                            offset: 1, color: 'rgba(58,132,255, 0)' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: '交换区使用量',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#FFDB5C' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#FFDB5C' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: '交换区剩余量',
                type: 'line',
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top'
                    }
                },
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#9FE6B8' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#9FE6B8' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            }
        ]
    });
}

/**
 *
 * @param value
 * @returns {string}
 */
function labelformatter(value) {
    return value+" GB"
}

/**
 *
 * @param value
 * @returns {string}
 */
function labelformatter1(value) {
    return value+" %"
}

/**
 * 占用内存情况
 */
function initSCMSMonitorMemData(){
    if(mac == null || mac == ""){
        return;
    }
    // var dialogWating = showWating("正在检索中...");
    $.ajax({
        url:ompModules+'/scms/monitorMem/mems',
        type:"POST",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:{mac:mac},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            var option =  memchart.getOption();
                            option.xAxis[0].data = result.data.xdata;
                            option.series[0].data = result.data.ydata0;
                            option.series[1].data = result.data.ydata1;
                            option.series[2].data = result.data.ydata2;
                            option.series[3].data = result.data.ydata3;
                            memchart.setOption(option);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
}


var cpuchart = null;
function initSCMSMonitorCpuChart() {
    cpuchart = echarts.init(document.getElementById('mainCpu'));
    cpuchart.setOption({
        title: {
            show:true,    // 是否显示标题组件,（true/false）
            text:'CPU',   // 主标题文本，支持使用\n换行
            textAlign:'auto',    //整体水平对齐（包括text和subtext）
            textVerticalAlign:'auto',//整体的垂直对齐（包括text和subtext）
            padding:0,    // 标题内边距 写法如[5,10]||[ 5,6, 7, 8] ,
            left:'auto',    // title组件离容器左侧距离，写法如'5'||'5%'
            right:'auto',    //'title组件离容器右侧距离
            top:'auto',    // title组件离容器上侧距离
            bottom:'auto',    // title组件离容器下侧距离
            borderColor: '#fff',     // 标题边框颜色
            borderWidth: 1,    // 边框宽度（默认单位px）
            textStyle: {    // 标题样式
                color: '#ccc',    //字体颜色
                fontStyle: 'normal',    //字体风格
                fontSize: 13,    //字体大小
                fontWeight: 400,    //字体粗细
                fontFamily: 'sans-serif',    //文字字体
                lineHeight: '13',    //字体行高
                align:'center',//文字水平对齐方式（left/right）
                verticalAlign:'middle',//文字垂直对齐方式（top/bottom）
            },
            subtext: '',    // 副标题
            subtextStyle: {    // 副标题样式
                color: '#ccc',
                fontStyle:'normal',
                fontWeight:'normal',
                fontFamily:'sans-serif',
                fontSize:11,
                lineHeight:11,
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        legend: {
            data: ['用户使用率', '系统使用率', '总使用率', '前空闲率']
        },
        /*toolbox: {
            feature: {
                saveAsImage: {}
            }
        },*/
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: []
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLabel:{
                    color: "#1e1e1e",
                    formatter: labelformatter1,
                    inside: false,
                    fontSize:'0.2rem',
                    fontWeight: 'bold'
                }
            }
        ],
        series: [
            {
                name: '用户使用率',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#e7bcf3' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#e7bcf3' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: '系统使用率',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: 'rgba(58,132,255, 0.5)' // 0% 处的颜色
                        }, {
                            offset: 1, color: 'rgba(58,132,255, 0)' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: '总使用率',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#FFDB5C' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#FFDB5C' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: '前空闲率',
                type: 'line',
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top'
                    }
                },
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#9FE6B8' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#9FE6B8' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            }
        ]
    });
}

/**
 * CPU情况
 */
function initSCMSMonitorCpuData(){
    if(mac == null || mac == ""){
        return;
    }
    // var dialogWating = showWating("正在检索中...");
    $.ajax({
        url:ompModules+'/scms/monitorCpu/cpu',
        type:"POST",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:{mac:mac},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            var option =  cpuchart.getOption();
                            option.xAxis[0].data = result.data.xdata;
                            option.series[0].data = result.data.ydata0;
                            option.series[1].data = result.data.ydata1;
                            option.series[2].data = result.data.ydata2;
                            option.series[3].data = result.data.ydata3;
                            cpuchart.setOption(option);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
}


/**
 *
 */
function closeWin() {
    mac = "";
    window.clearInterval(timer_);
    search('datatables');
}