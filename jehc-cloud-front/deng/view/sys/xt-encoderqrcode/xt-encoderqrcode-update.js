//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-encoderqrcode/xt-encoderqrcode-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtEncoderqrcode(){
	submitBForm('defaultForm',sysModules+'/xtEncoderqrcode/update',base_html_redirect+'/sys/xt-encoderqrcode/xt-encoderqrcode-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

/**初始化附件右键菜单开始 参数4为1表示不拥有上传和删除功能 即明细页面使用**/
initBFileRight('xt_attachment_id','xt_attachment_id_pic',2);
/**初始化附件右键菜单结束**/

$(document).ready(function(){
	var xt_encoderqrcode_id = GetQueryString("xt_encoderqrcode_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtEncoderqrcode/get/"+xt_encoderqrcode_id,{},function(result){
        $("#xt_encoderqrcode_id").val(result.data.xt_encoderqrcode_id);
        $("#title").val(result.data.title);
        $("#url").val(result.data.url);
        $("#xt_attachment_id").val(result.data.xt_attachment_id); 
        $("#content").val(result.data.content);
        /**配置附件回显方法开始**/
        var params = {xt_attachment_id:$('#xt_attachment_id').val(),field_name:'xt_attachment_id'};
        ajaxBFilePathBackRequest(fileModules+'/xtCommon/attAchmentPathpp',params);
        /**配置附件回显方法结束**/
    });
});