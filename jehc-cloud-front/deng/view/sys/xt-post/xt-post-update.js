//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-post/xt-post-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtPost(){
	submitBForm('defaultForm',sysModules+'/xtPost/update',base_html_redirect+'/sys/xt-post/xt-post-list.html',null,"PUT");
}


$(document).ready(function(){
	var xt_post_id = GetQueryString("xt_post_id");
	//加载表单数据
	ajaxBRequestCallFn(sysModules+"/xtPost/get/"+xt_post_id,{},function(result){
	    $("#xt_departinfo_id").val(result.data.xt_departinfo_id);
	    $("#xt_departinfo_name").val(result.data.xt_departinfo_name);
        $("#p_code").val(result.data.p_code);
	    $("#xt_post_id").val(result.data.xt_post_id);
	    $("#xt_post_parentId").val(result.data.xt_post_parentId);
	    $("#xt_post_desc").val(result.data.xt_post_desc);
	    $("#xt_post_name").val(result.data.xt_post_name);
	    $("#xt_post_maxNum").val(result.data.xt_post_maxNum);
	    $("#xt_post_grade").val(result.data.xt_post_grade);
	    if($('#xt_post_parentId').val() != "" && $('#xt_post_parentId').val() != null && $('#xt_post_parentId').val() != 0){
	    	 $.ajax({
	  		   type:"GET",
	  		   url:sysModules+"/xtPost/get/"+$('#xt_post_parentId').val(),
	  		   success: function(result){
	  			   var res = result;
	  			   if(res != null && res != ''){
	  				   $('#xt_post_parentName').val(res.data.xt_post_name);
	  			   }else{
	  				   $('#xt_post_parentName').val("无");
	  				   $('#xt_post_parentId').val("0");
	  			   }
	  		   }
	  		});
	    }
	});

});



/////////////////////部门选择器开始///////////////////
function departSelect(){
	$('#departSelectModal').modal();
	var setting = {
		view:{
			selectedMulti:false
		},
		check:{
			enable:false
		},
		// async:{
		// 	type:'GET',
		// 	enable:true,//设置 zTree是否开启异步加载模式  加载全部信息
		// 	url:sysModules+"/xtdepartinfo/bztree",//Ajax获取数据的 URL地址
		// 	otherParam:{
		// 	'expanded':function(){return 'true'}
		// 	} //异步参数
		// },
		data:{
			//必须使用data  
			simpleData:{
				enable:true,
				idKey:"id",//id编号命名 默认  
				pIdKey:"pId",//父id编号命名 默认   
				rootPId:0 //用于修正根节点父节点数据，即 pIdKey 指定的属性值  
			}
		},
		edit:{
			enable:false
		},  
		callback:{  
			onClick:onClick//单击事件
		}  
	};
    var zTreeNodes;
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    ajaxBRequestCallFn(sysModules+"/xtDepartinfo/bztree",null,function(result){
        zTreeNodes = eval("(" + result.data + ")");
        treeObj = $.fn.zTree.init($("#tree"), setting,zTreeNodes);
        closeWating(null,dialogWating);
    },null,"GET");
}

//单击事件
function onClick(event, treeId, treeNode, msg){  
}  
function doDepartSelect(){
	var zTree = $.fn.zTree.getZTreeObj("tree"),
	nodes = zTree.getSelectedNodes();
	if (nodes.length != 1) {
		toastrBoot(4,"请选择一条隶属部门信息");
		return;
	}
	msgTishCallFnBoot("确定要选择【<font color=red>"+nodes[0].name+"</font>】？",function(){
		$('#xt_departinfo_name').val(nodes[0].name);
		$('#xt_departinfo_id').val(nodes[0].id);
		$('#xt_post_parentName').val("无");
		$('#xt_post_parentId').val("0");
		$('#departSelectModal').modal('hide');
	})
}
/////////////////////部门选择器结束///////////////////

/////////////////////岗位选择器开始///////////////////
function postSelect(){
	var xt_departinfo_id = $('#xt_departinfo_id').val();
	if(xt_departinfo_id ==  null || xt_departinfo_id == ""){
		toastrBoot(4,"请选择隶属部门！");
		return;
	}
	$('#postSelectModal').modal();
	var setting = {
		view:{
				selectedMulti:false
			},
			check:{
				enable:false
			},
			// async:{
			// 	type:'GET',
			// 	enable:true,//设置 zTree是否开启异步加载模式  加载全部信息
			// 	url:sysModules+"/xtpost/bztree/"+$('#xt_departinfo_id').val(),//Ajax获取数据的 URL地址
			// 	otherParam:{
			// 	'expanded':function(){return 'true'}
			// 	} //异步参数
			// },
			data:{
				//必须使用data  
				simpleData:{
				enable:true,
				idKey:"id",//id编号命名 默认  
				pIdKey:"pId",//父id编号命名 默认   
				rootPId:0 //用于修正根节点父节点数据，即 pIdKey 指定的属性值  
			}
		},
		edit:{
			enable:false
		},  
		callback:{  
			onClick:onClick//单击事件
		}  
	};
    var zTreeNodes;
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    ajaxBRequestCallFn(sysModules+"/xtPost/bztree/"+$('#xt_departinfo_id').val(),null,function(result){
        zTreeNodes = eval("(" + result.data + ")");
        treeObj = $.fn.zTree.init($("#posttree"), setting,zTreeNodes);
        closeWating(null,dialogWating);
    },null,"GET");
}

function doPostSelect(){
	var zTree = $.fn.zTree.getZTreeObj("posttree"),
	nodes = zTree.getSelectedNodes();
	if (nodes.length != 1) {
		toastrBoot(4,"请选择一条上级岗位信息");
		return;
	}
	
//	if(nodes[0].id == $("#xt_post_id").val()){
//		toastrBoot(4,"不能选择该记录");
//		return;
//	}
	
	var node = getZtreeCurrendIdNode($("#xt_post_id").val(),zTree);
	if(!validateZtreeNodeInCurrentNode(nodes[0].id,node)){
		toastrBoot(4,"不能选择该记录");
		return;
	}
	
	msgTishCallFnBoot("确定要选择【<font color=red>"+nodes[0].name+"</font>】？",function(){
		$('#xt_post_parentName').val(nodes[0].name);
		$('#xt_post_parentId').val(nodes[0].id);
		$('#postSelectModal').modal('hide');
	})
}
/////////////////////岗位选择器结束///////////////////