//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-ip-frozen/xt-ip-frozen-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtIpFrozen(){
	submitBForm('defaultForm',sysModules+'/xtIpFrozen/update',base_html_redirect+'/sys/xt-ip-frozen/xt-ip-frozen-list.html',null,"PUT");
}

$(document).ready(function(){
	datetimeInit();
	var xt_ip_frozen_id = GetQueryString("xt_ip_frozen_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtIpFrozen/get/"+xt_ip_frozen_id,{},function(result){
        $("#xt_ip_frozen_id").val(result.data.xt_ip_frozen_id);
        $("#address").val(result.data.address);
        $("#status").val(result.data.status);
        $("#content").val(result.data.content);
    });
});
