//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-ip-frozen/xt-ip-frozen-list.html');
}


$(document).ready(function(){
	datetimeInit();
	var xt_ip_frozen_id = GetQueryString("xt_ip_frozen_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtIpFrozen/get/"+xt_ip_frozen_id,{},function(result){
        $("#xt_ip_frozen_id").val(result.data.xt_ip_frozen_id);
        $("#address").val(result.data.address);
        $("#status").val(result.data.status);
        $("#content").val(result.data.content);

        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
    });
});
