//分配资源
function addOauthResources(xt_role_id,xt_role_name){
    var SourcesModalCount = 0 ;
	$('#XtMenuinfoBody').height(reGetBodyHeight()-288);
	$('#XtMenuinfoModal').modal({backdrop:'static',keyboard:false});
	$('#XtMenuinfoModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#XtMenuinfoModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth() * 0.9+'px'});
        if(++SourcesModalCount == 1) {
            $('#XtMenuinfoModalLabel').html("角色权限--->导入资源---><font color=red>" + xt_role_name + "</font>");
            $('#roleIdForMenu').val(xt_role_id);
            var setting = {
                view: {
                    selectedMulti: false,
                    showLine: false
                },
                check: {
                    enable: true,
                    chkboxType: {"Y": "ps", "N": ""}
                    /* chkboxType: { “Y”: “ps”, “N”: “ps” }
                    Y 属性定义 checkbox 被勾选后的情况；
                    N 属性定义 checkbox 取消勾选后的情况；
                    “p” 表示操作会影响父级节点；
                    “s” 表示操作会影响子级节点。 */
                },
                data: {
                    //必须使用data
                    simpleData: {
                        enable: true,
                        idKey: "id",//id编号命名 默认
                        pIdKey: "pId",//父id编号命名 默认
                        rootPId: 0 //用于修正根节点父节点数据，即 pIdKey 指定的属性值
                    }
                },
                edit: {
                    enable: false
                },
                callback: {}
            };
            InitztData(setting, xt_role_id);
        }
    });  
}
//保存分配菜单
function addXtMR(){
	var treeObj = $.fn.zTree.getZTreeObj("tree");
    var nodes = treeObj.getCheckedNodes(true);
    //var msg = "name--id--pid\n";
    var id;
    for (var i = 0; i < nodes.length; i++) {
        //msg += nodes[i].name+"--"+nodes[i].id+"--"+nodes[i].pId+"\n";
    	if(id == null || id == ''){
			id = nodes[i].id;
		}else{
			id = id+','+nodes[i].id;
		}
    }
    msgTishCallFnBoot("确定保存分配的资源？",function(){
    	var params = {sources_id:id,role_id:$('#roleIdForMenu').val()};
		ajaxBRequestCallFn(oauthModules+'/oauthRole/saveOauthRR',params,function(){
			toastrBoot(3,"保存分配资源成功");
		},null,"PUT");
		
	})
}
//初始数据
var zTreeNodes;
function InitztData(setting,role_id) {
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    ajaxBRequestCallFn(oauthModules+'/oauthRole/get/'+role_id,{},function(result){
        var sysmode_id = result.data.sysmode_id
        ajaxBRequestCallFn(oauthModules+"/oauthRole/resources?role_id="+role_id+"&sysmode_id="+sysmode_id,null,function(result){
            zTreeNodes = eval("(" + result.data + ")");
            treeObj = $.fn.zTree.init($("#tree"), setting,zTreeNodes);
            treeObj.expandAll(true);
            closeWating(null,dialogWating);
        },null,"GET");
    });

}
function closeXtMenuinfoWin(){
	$("#XtMenuinfoModal").modal('hide');//手动关闭
}