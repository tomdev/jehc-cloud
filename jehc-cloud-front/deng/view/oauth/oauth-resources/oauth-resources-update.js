function updateXtMenuinfo(value){
    $('#updateXtMenuinfoForm')[0].reset();
    $('#updateXtMenuinfoForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    $.ajax({
        type:"GET",
        url:oauthModules+'/oauthResources/get/'+value,
        success: function(result){
            result = result.data;
            $("#updateXtMenuinfoForm").find("#resources_id").val(result.resources_id);
            $("#updateXtMenuinfoForm").find("#resources_title").val(result.resources_title);
            $("#updateXtMenuinfoForm").find("#resources_parentid_").val(result.resources_parentid);
            $("#updateXtMenuinfoForm").find("#resources_parentidTitle_").val(result.resources_parentidTitle_);
            $("#updateXtMenuinfoForm").find("#resources_leaf").val(result.resources_leaf);
            $("#updateXtMenuinfoForm").find("#resources_sys").val(result.resources_sys);
            $("#updateXtMenuinfoForm").find("#resources_iconCls").val(result.resources_iconCls);
            $("#updateXtMenuinfoForm").find("#resources_url").val(result.resources_url);
            $("#updateXtMenuinfoForm").find("#resources_sort").val(result.resources_sort);
            $("#updateXtMenuinfoForm").find("#resources_status").val(result.resources_status);
            $("#updateXtMenuinfoForm").find("#resources_module_idTemp").val(result.resources_module_id);
            InitMenuModuleListSetV('resources_module_id_','resources_module_idTemp');
            $('#updateXtMenuinfoModal').modal({"backdrop":"static"});
        }
    });
}

function doUpdateXtMenuinfo(){
    submitBFormCallFn('updateXtMenuinfoForm',oauthModules+'/oauthResources/update',function(result){
        try {
            if(typeof(result.success) != "undefined"){
                if(result.success){
                    window.parent.toastrBoot(3,result.message);
                    initSysModeTable();
                    $('#updateXtMenuinfoModal').modal('hide');
                }else{
                    window.parent.toastrBoot(4,result.message);
                }
            }
        } catch (e) {

        }
    },null,"PUT");
}