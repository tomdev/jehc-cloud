//弹新增窗体
function addXtMenuinfo(value,valueText){
    $('#addXtMenuinfoForm')[0].reset();
    if(value != null && typeof(value) != 'undefined'){
        $('#resources_parentid').val(value);
    }else{
        $('#resources_parentid').val("0");
    }
    if(valueText != null && typeof(valueText) != 'undefined'){
        $('#resources_parentTitle').val(valueText);
    }else{
        $('#resources_parentTitle').val("一级资源");
    }
    $('#addXtMenuinfoForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    InitMenuModuleListSetV('resources_module_id');
    $('#addXtMenuinfoModal').modal({"backdrop":"static"});
}
//执行新增操作
function doAddXtMenuinfo(){
    submitBFormCallFn('addXtMenuinfoForm',oauthModules+'/oauthResources/add',function(result){
        try {
            result = result;
            if(typeof(result.success) != "undefined"){
                if(result.success){
                    window.parent.toastrBoot(3,result.message);
                    initSysModeTable();
                    $('#addXtMenuinfoModal').modal('hide');
                }else{
                    window.parent.toastrBoot(4,result.message);
                }
            }
        } catch (e) {

        }
    });
}