//返回
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-key-info/oauth-key-info-list.html');
}
$(document).ready(function(){
	var key_info_id = GetQueryString("key_info_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthKeyInfo/get/'+key_info_id,{},function(result){
		$('#key_info_id').val(result.data.key_info_id);
		$('#key_name').val(result.data.key_name);
        $('#title').val(result.data.title);
		$('#key_pass').val(result.data.key_pass);
		$('#key_exp_date').val(result.data.key_exp_date);
		$('#isUseExpDate').val(result.data.isUseExpDate);
		$('#status').val(result.data.status);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);

	});
});
