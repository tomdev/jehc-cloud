
// function beforeClick(treeId, treeNode) {
//     if (treeNode.level == 0 ) {
//         var zTree = $.fn.zTree.getZTreeObj("tree");
//         zTree.expandNode(treeNode);
//         return false;
//     }
//     return true;
// }


//加载数据完成事件
// function onAsyncSuccess(event, treeId, treeNode, msg){
//     // var length = $('#keyword').val().length;
//     // if(length > 0){
//     // 	var zTree = $.fn.zTree.getZTreeObj(treeId);
//     //     var nodeList = zTree.getNodesByParamFuzzy("name", $('#keyword').val());
//     //     //将找到的nodelist节点更新至Ztree内
//     //     $.fn.zTree.init($("#"+treeId), setting, nodeList);
//     // }
//     // closeWating(null,dialogWating);
// }

//单击事件
function onClick(event, treeId, treeNode, msg){
}

// /**
//  * 搜索树，显示并展示
//  * @param treeId
//  * @param text文本框的id
//  */
// function filter(){
//     InitztData();
// }

//删除
function delXtFunctioninfo(id){
    // var zTree = $.fn.zTree.getZTreeObj("tree"),
    //     nodes = zTree.getSelectedNodes();
    // if (nodes.length != 1) {
    //     toastrBoot(4,"必须选择一条记录进行删除");
    //     return;
    // }
    // if(nodes[0].tempObject != 'Function'){
    //     toastrBoot(4,"选择的记录必须为功能才能删除");
    //     return;
    // }
    // var params = {xt_functioninfo_id:nodes[0].id, _method:'DELETE'};
    var params = {function_info_id:id, _method:'DELETE'};
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        ajaxBRequestCallFn(oauthModules+'/oauthFunctionInfo/delete',params,function(result){
            try {
                if(typeof(result.success) != "undefined"){
                    if(result.success){
                        window.parent.toastrBoot(3,result.message);
                    }else{
                        window.parent.toastrBoot(4,result.message);
                    }
                    initTreeTable();
                }
            } catch (e) {

            }
        },null,"DELETE");
    })
}



/**
 * 下拉框变化筛选菜单树
 * @param thiz
 */
function doSysModeEvent(thiz) {
    var setting = {
        view: {
            // showLine: true,
            // showIcon: true,
            selectedMulti: false,
            dblClickExpand: true
        },
        data:{
            //必须使用data
            simpleData:{
                enable:true,
                idKey:"id",//id编号命名 默认
                pIdKey:"pId",//父id编号命名 默认
                rootPId:0 //用于修正根节点父节点数据，即 pIdKey 指定的属性值
            }
        },
        edit:{
            enable:false
        },
        callback:{
            // beforeClick: beforeClick,
            onClick:onClick//,//单击事件
            // onAsyncSuccess:onAsyncSuccess//加载数据完成事件
        }
    };

    var zTreeNodes;
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    var parm = {"sysmode_id":thiz.value};
    ajaxBRequestCallFn(oauthModules+'/oauthResources/bZTree',parm,function(result){
        zTreeNodes = eval("(" + result.data + ")");
        if(undefined == result.data || null == result.data || result.data == "" || result.data == "[]"){
            treeObj = $.fn.zTree.init($("#menu"), setting,[]);
            $(".ztree").append("暂无数据");
        }else{
            treeObj = $.fn.zTree.init($("#menu"), setting,zTreeNodes);
        }

        // treeObj.expandAll(true);

        // var t = $("#menu");
        // t.hover(function () {
        //     if (!t.hasClass("showIcon")) {
        //         t.addClass("showIcon");
        //     }
        // }, function() {
        //     t.removeClass("showIcon");
        // });
        closeWating(null,dialogWating);
    },null,"GET");
}

/////////////////////模块选择器开始///////////////////
function xtMenuinfoSelect(flag){
    $('#flag').val(flag);
    $('#xtMenuinfoBody').height(300);
    var $modal_dialog = $("#xtMenuinfoSelectDialog");
    $modal_dialog.css({'width':'500px'});
    $('#xtMenuinfoSelectModal').modal({"backdrop":"static"});
    treeObj = $.fn.zTree.init($("#menu"), null,[]);
    $(".ztree").append("暂无数据");
    initComboData("sysmode_id_",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname");

}

//单击事件
function onClick(event, treeId, treeNode, msg){

}

function doXtMenuinfoSelect(){
    var zTree = $.fn.zTree.getZTreeObj("menu"),
        nodes = zTree.getSelectedNodes();
    if (nodes.length != 1) {
        toastrBoot(4,"请选择一条隶属模块信息");
        return;
    }
    msgTishCallFnBoot("确定要选择【<font color=red>"+nodes[0].name+"</font>】？",function(){
        if($('#flag').val() == 1){
            $("#addXtFunctioninfoForm").find('#resources_title').val(nodes[0].name);
            $("#addXtFunctioninfoForm").find('#menu_id_').val(nodes[0].id);
        }else if($('#flag').val() == 2){
            $("#updateXtFunctioninfoForm").find('#resources_title').val(nodes[0].name);
            $("#updateXtFunctioninfoForm").find('#menu_id_').val(nodes[0].id);
        }
        $('#xtMenuinfoSelectModal').modal('hide');
    })
}
/////////////////////模块选择器结束///////////////////

$(function () {
    initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname");
    initTreeTable();
})

function doThisEvent(thiz){
    initTreeTable();
}

function initTreeTable(){
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    $.ajax({
        url:oauthModules+'/oauthFunctionInfo/treeList?sysmode_id='+$('#sysmode_id').val()+'&resources_module_id='+$("#resources_module_id").val(),
        type:"GET",
        dataType:"json",
        success:function(data) {
            var $table = $("#table");
            console.log(data);
            data = eval("(" + data.data + ")");
            console.log(data);
            $table.bootstrapTable('destroy').bootstrapTable({
                data:data,
                striped:true,
                class:'table table-hover table-bordered',
                sidePagination:"client",//表示服务端请求
                pagination:false,
                treeView:true,
                treeId:"id",
                treeField:"name",
                height: $(window).height() - 200 ,
                sortable:false,//是否启用排序
                columns:[
                    /*
                    {
                        field: 'ck',
                        checkbox:true
                    },
                    */
                    {
                        field:'name',
                        title:'名称'
                    },
                    {
                        field:'tempObject',
                        title:'性质',
                        formatter:'typeFormatter'
                    },
                    {
                        field:'integerappend',
                        title:'数据权限',
                        formatter:'authorFormatter'
                    },
                    {
                        field:'integerappend',
                        title:'拦截类型',
                        formatter:'authorTypeFormatter'
                    },
                    {
                        field:'buessid',
                        title:'操作',
                        formatter:'btnFormatter'
                    }
                ],
                onLoadSuccess:function(data){

                }
            });
            closeWating(null,dialogWating);
            if( $("#expandTreeState").val() == 1){
                expandTree();
            }
            if( $("#expandTreeState").val() == 0){
                collapseTree();
            }
        }
    });
}

/**
 *
 * @param value
 * @param row
 * @param index
 */
function typeFormatter(value, row, index){
    var tempObject = row.tempObject;
    if(tempObject == 'Sources'){
        return "<span class='label label-lg font-weight-bold label-light-primary label-inline'>资源</span>";
    }
    if(tempObject == 'Function'){
        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>功能</span>";
    }
}

/**
 * 数据权限
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
function authorFormatter(value, row, index){
    var integerappend = row.integerappend;
    if(integerappend != null && integerappend != ""){
        var val = integerappend.split(",");
        if(val[0] == 0){
            return "<font color='red'>是</font>";
        }else if(val[0] == 1){
            return "否";
        }
    }
}

/**
 * 拦截类型
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
function authorTypeFormatter(value, row, index){
    var val = value.split(",");
    if(val[1] == 0){
        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>无需拦截</span>";
    }else if(val[1] == 1){
        return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>必须拦截</span>"
    }
}

/**
 * 格式化按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
function btnFormatter(value, row, index){
    var tempObject = row.tempObject;
    var name = row.name;
    var content = row.content;
    if(tempObject == 'Sources'){
        // return '<a href=javascript:addXtMenuinfo("'+value+'","'+content+'") class="btn btn-secondary m-btn m-btn--custom m-btn--icon" title="配置其菜单资源下的功能">添加功能</a>';
    }
    if(tempObject == 'Function'){
        return '<a href=javascript:updateXtFunctioninfo("'+value+'") title="编 辑" style="padding: 20px 30px" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" ><i class="la la-pencil"></i></a><a href=javascript:delXtFunctioninfo("'+value+'")   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="删 除"><i class="la la-remove"></i></span></a>';
    }
}

function expandTree(){
    $("#expandTreeState").val("1");
    $('#table').bootstrapTable("expandAllTree")
}
function collapseTree(){
    $("#expandTreeState").val("0");
    $('#table').bootstrapTable("collapseAllTree")
}
