var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthAccount/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 3
        },
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"account_id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"account_id",
                width:"50px"
            },
            {
                data:"account_id",
                width:"50px",
                render:function(data, type, row, meta) {
                    var account = row.account;
                    var name = row.name;
                    var status = row.status;
                    var btn = '<button onclick=toOauthAccountDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情">' +
                        '<i class="m-menu__link-icon la la-eye""></i>' +
                        '</button>';
                    btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=initRoleinfo("'+data+'") title="角色权限"><i class="fa flaticon-avatar"></i></button> ';
                    btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=resetAccountPwd("'+data+'","'+account+'","'+name+'") title="重置密码"><i class="fa fa-gears"></i></button>';

                    if(status == 1){
                        btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=unfreeze("'+data+'") title="解冻"><i class="fa flaticon-cogwheel-2"></i></button> ';
                    }
                    return btn;
                }
            },
            {
                data:'account',
                width:"50px"
            },
            {
                data:'name',
                width:"50px"
            },
            {
                data:'account_type_text',
                width:"50px",
                render:function(data, type, row, meta) {
                    var oauthAccountTypes  = row.oauthAccountTypes;
                    var account_type_text = null;
                    if(null != oauthAccountTypes){
                        for(var i =0; i < oauthAccountTypes.length;i++){
                            console.info(oauthAccountTypes[i])
                            if(null == account_type_text){
                                account_type_text = oauthAccountTypes[i].title;
                            }else{
                                account_type_text = account_type_text+","+oauthAccountTypes[i].title;
                            }
                        }
                    }
                    return "<button class=\"btn btn-light-primary m-btn m-btn--custom m-btn--icon\" data-toggle=\"popover\" data-original-title=\"提示*\" aria-describedby=\"popover166048\" data-content='"+account_type_text+"'>查看</button>";
                }
            },
            {
                data:'status',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>正常</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>冻结</span>"
                    }
                    if(data == 2){
                        return "<span class='m-badge m-badge--danger'>禁用</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
            },
            {
                data:'email',
                width:"100px"
            },
            {
                data:'create_time',
                width:"50px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:'update_time',
                width:"50px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:'last_login_time',
                width:"50px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');

    setTimeout(function () {
        // $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    },500)

});
//新增
function toOauthAccountAdd(){
    tlocation(base_html_redirect+'/oauth/oauth-account/oauth-account-add.html');
}
//修改
function toOauthAccountUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/oauth/oauth-account/oauth-account-update.html?account_id='+id);
}
//详情
function toOauthAccountDetail(id){
    tlocation(base_html_redirect+'/oauth/oauth-account/oauth-account-detail.html?account_id='+id);
}
//冻结
function freeze(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要冻结的数据");
        return;
    }
    msgTishCallFnBoot("确定要冻结所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {/*account_id:id,*/_method:'DELETE'};
        ajaxBReq(oauthModules+'/oauthAccount/freeze/'+id,params,['datatables'],null,"DELETE");
    })
}

/**
 * 解冻
 * @param account_id
 */
function unfreeze(account_id){
    msgTishCallFnBoot("确定要解冻该账户？",function(){
        var params = {/*account_id:id,*/_method:'DELETE'};
        ajaxBReq(oauthModules+'/oauthAccount/unfreeze/'+account_id,params,['datatables'],null,"DELETE");
    })
}

//初始化类型
$(document).ready(function(){
    /*$("#account_type_ids").select2();*/
    initComboDataCallFn("account_type_ids",oauthModules+"/oauthAccountType/listAll","account_type_id","title",null,function (data) {
        var account_type_idsSelect = $("#account_type_ids").select2({
            width: "100%", //设置下拉框的宽度
            placeholder: "请选择", // 空值提示内容，选项值为 null
            tags:true,
            /*allowClear:!0,*/
            createTag:function (decorated, params) {
                return null;
            }
        });

        $("#account_type_ids").on("select2:select",function(){
            var data = $(this).val();
            console.log(data);
            var account_type_ids = $('#account_type_ids').val();
            var ids = null;
            if(null != account_type_ids){
                for(var i = 0; i < account_type_ids.length; i++){
                    if(ids ==  null){
                        ids = account_type_ids[i];
                    }else{
                        ids = ids+","+account_type_ids[i];
                    }
                }
                $('#account_type_id').val(ids);
            }
        });
    });
});

function resetForm() {
    $("#account_type_ids").val("").select2();
    resetAll(); console.info($("#account_type_id").val())

}

function searchForm() {
    var account_type_ids = $('#account_type_ids').val();
    var ids = null;
    if(null != account_type_ids){
        for(var i = 0; i < account_type_ids.length; i++){
            if(ids ==  null){
                ids = account_type_ids[i];
            }else{
                ids =ids+ ","+account_type_ids[i];
            }
        }
    }
    $('#account_type_id').val(ids);
    search('datatables');
}




//角色权限
function initRoleinfo(id){
    var userRoleModalCount = 0;
    $('#userRoleModal').modal({backdrop: 'static', keyboard: false});
    $('#userRoleModal').on("shown.bs.modal",function(){
        if(++userRoleModalCount == 1){
            var opt = {
                searchformId:'searchFormRole'
            };
            var options = DataTablesList.listOptions({
                ajax:function (data, callback, settings){datatablesListCallBack(data, callback, settings,oauthModules+'/oauthRole/roleList/'+id,opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                tableHeight:'200px',
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        data:"account_role_id",
                        width:'20px',
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUserRole" value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        width:'20px',
                        data:"account_role_id"
                    },
                    {
                        data:'role_name'
                    }
                ]
            });
            $('#userRoleDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallUserRole','checkchildUserRole');
            //实现单击行选中
            clickrowselected('userRoleDatatables');
        }
    });
}


//重置密码
function resetAccountPwd(id,account,name){
    msgTishCallFnBoot('确定要重置<br>姓名：'+name+'<br>登录账户：'+account+'的密码？',function(){
        var params = {account_id:id};
        ajaxBReq(oauthModules+'/oauthAccount/restPwd',params,['datatables'],null,"POST");
    })
}
