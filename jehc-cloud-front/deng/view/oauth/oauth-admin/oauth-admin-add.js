
function addAdmin(sys_mode_id){
    $('#searchFormUnImportU')[0].reset();
    var UserinfoModalCount = 0 ;
    $('#addAdminBody').height(reGetBodyHeight()-128);
    $('#addAdminModal').modal({backdrop:'static',keyboard:false});
    $('#addAdminModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#addAdminDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++UserinfoModalCount == 1){
            $('#searchFormUnImportU')[0].reset();
            $("#sys_mode_id").val(sys_mode_id);
            initAccountTable();
            initComboData("account_type_id_",oauthModules+"/oauthAccountType/listAll","account_type_id","title");
        }
    });
}

function doAddAdmin() {
    if(returncheckedLength('checkchildUnImportU') <= 0){
        toastrBoot(4,"请选择要设置的管理员账号");
        return;
    }
    msgTishCallFnBoot("确定要设置所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {account_id:id,level:$("#level").val(),sys_mode_id: $("#sys_mode_id").val(),_method:'POST'};
        ajaxBReq(oauthModules+'/oauthAdmin/add',params,['datatables'],null,"POST");
    })
}

function initAccountTable(){
    var opt1 = {
        searchformId:'searchFormUnImportU'
    };
    var options1 = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthAccount/list',opt1);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        //dom:'<"top"i>rt<"bottom"flp><"clear">',
        tableHeight:'100px',
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"account_id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUnImportU" value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"account_id",
                width:"50px"
            },
            {
                data:'account'
            },
            {
                data:'name'
            },
            {
                data:'account_type_text'
            },
            {
                data:'email'
            }
        ]
    });
    $('#AccountDatatables').dataTable(options1);
    //实现全选反选
    docheckboxall('checkallUnImportU','checkchildUnImportU');
    //实现单击行选中
    clickrowselected('AccountDatatables');
}