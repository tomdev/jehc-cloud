package jehc.cloud.gateway.route.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * @Desc 网关路由客户端连接
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
public class GatewayNettyClient implements ApplicationEventPublisherAware {
    private EventLoopGroup group = new NioEventLoopGroup();

    @Value("${jehc.cloud.route.host:127.0.0.1}")
    private String host;

    @Value("${jehc.cloud.route.port:22006}")
    private Integer port;

    @Autowired
    private GatewayNettyClient nettyClient;

    @Value("${jehc.cloud.route.clientId:GatewayRoteId}")
    private String clientId;//客户端id（每个服务对应一个客户端唯一id）

    @Value("${jehc.cloud.route.clientGroupId:GatewayGroupId}")
    private String clientGroupId;//组Id（可以存多个服务共享一个组Id）

    private SocketChannel socketChannel;

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * 发送消息
     */
    public void send(String message) {
        socketChannel.writeAndFlush(message);
    }


    @PostConstruct
    public void start() {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                 .channel(NioSocketChannel.class)
                 .remoteAddress(host, port)
                 .option(ChannelOption.SO_KEEPALIVE, true)
                 .option(ChannelOption.TCP_NODELAY, true)
                 .handler(new GatewayNettyClientInitializer(nettyClient,clientGroupId,clientId,applicationEventPublisher));
        ChannelFuture future = bootstrap.connect();
        //客户端断线重连逻辑
        future.addListener((ChannelFutureListener) future1 -> {
            if (future1.isSuccess()) {
                log.info("连接网关路由Netty服务端成功");
            } else {
                log.info("连接网关路由失败，进行断线重连");
                future1.channel().eventLoop().schedule(() -> start(), 20, TimeUnit.SECONDS);
            }
        });
        socketChannel = (SocketChannel) future.channel();
        log.info("网关路由通道号："+socketChannel.id());
    }
}
