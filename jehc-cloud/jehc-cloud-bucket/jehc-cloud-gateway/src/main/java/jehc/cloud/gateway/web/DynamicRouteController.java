package jehc.cloud.gateway.web;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * @Desc 网关路由API
 * @Author 邓纯杰
 * @CreateTime 2017-10-1 13:26:15
 */
@RestController
@RequestMapping("/gateWayRoute")
@Api(value = "网关路由API",tags = "网关路由API",description = "网关路由API")
public class DynamicRouteController {
    @Autowired
    private RouteDefinitionLocator routeDefinitionLocator;

    /**
     * 查询所有网关路由信息
     * @return
     */
    @RequestMapping("/routes")
    public Flux<RouteDefinition> getRouteDefinitions(){
        return routeDefinitionLocator.getRouteDefinitions();
    }
}
