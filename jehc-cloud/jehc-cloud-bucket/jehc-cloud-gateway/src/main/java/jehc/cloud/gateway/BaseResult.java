package jehc.cloud.gateway;
/**
 * @Desc 返回结果接收对象
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class BaseResult<T>{
    private int status = 200;
    private String message ="操作成功";
    private Boolean success = true;
    private Object data;
    private T obj;
    /**
     * constructs a successful result
     */
    public BaseResult() {
        success = true;
    }

    public BaseResult(Object data){
        this.success = true;
        this.data = data;
    }
    public BaseResult(String message, Boolean success){
        this.message = message;
        this.success = success;
    }
    public BaseResult(String message, Boolean success, Object data){
        this.message = message;
        this.success = success;
        this.data = data;
    }
    public BaseResult(int status, String message, Boolean success){
        this.status = status;
        this.message = message;
        this.success = success;
    }

    public BaseResult(int status, String message, Boolean success, Object data){
        this.status = status;
        this.message = message;
        this.success = success;
        this.data = data;
    }
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }

    public static BaseResult success(Object data) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(200);
        baseResult.setMessage("操作成功");
        baseResult.setData(data);
        baseResult.setSuccess(true);
        return baseResult;
    }

    /**
     *
     * @param message
     * @return
     */
    public static BaseResult success(String message) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(200);
        baseResult.setMessage(message);
        baseResult.setSuccess(true);
        return baseResult;
    }

    /**
     *
     * @return
     */
    public static BaseResult success() {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(200);
        baseResult.setMessage("操作成功");
        baseResult.setSuccess(true);
        return baseResult;
    }

    /**
     *
     * @param data
     * @return
     */
    public static BaseResult fail(Object data) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(500);
        baseResult.setMessage("操作失败");
        baseResult.setData(data);
        baseResult.setSuccess(false);
        return baseResult;
    }

    /**
     *
     * @param message
     * @return
     */
    public static BaseResult fail(String message) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(500);
        baseResult.setMessage(message);
        baseResult.setSuccess(false);
        return baseResult;
    }

    /**
     *
     * @return
     */
    public static BaseResult fail() {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(500);
        baseResult.setMessage("操作失败");
        baseResult.setSuccess(false);
        return baseResult;
    }
}
