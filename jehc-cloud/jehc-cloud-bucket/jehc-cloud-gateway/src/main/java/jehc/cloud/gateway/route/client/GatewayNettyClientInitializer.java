package jehc.cloud.gateway.route.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import jehc.cloud.gateway.route.client.handler.GatewayNettyClientHandler;
import org.springframework.context.ApplicationEventPublisher;

/**
 * @Desc 服务器网关路由通道初始化
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class GatewayNettyClientInitializer extends ChannelInitializer<SocketChannel> {

    private GatewayNettyClient gatewayNettyClient;

    private String clientId;//客户端id（每个服务对应一个客户端唯一id）

    private String clientGroupId;//组Id（可以存多个服务共享一个组Id）

    private ApplicationEventPublisher applicationEventPublisher;

    public GatewayNettyClientInitializer(){

    }

    public GatewayNettyClientInitializer(GatewayNettyClient gatewayNettyClient, String clientGroupId, String clientId,ApplicationEventPublisher applicationEventPublisher){
        this.gatewayNettyClient = gatewayNettyClient;
        this.clientGroupId = clientGroupId;
        this.clientId = clientId;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        socketChannel.pipeline().addLast("decoder", new StringDecoder());
        socketChannel.pipeline().addLast("encoder", new StringEncoder());
        socketChannel.pipeline().addLast(new GatewayNettyClientHandler(gatewayNettyClient,clientGroupId,clientId,applicationEventPublisher));
    }
}