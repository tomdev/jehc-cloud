package jehc.cloud.sentinel;

import com.alibaba.csp.sentinel.cluster.server.ClusterTokenServer;
import com.alibaba.csp.sentinel.cluster.server.SentinelDefaultTokenServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * @Desc 启动应用
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@SpringBootApplication
@ComponentScan("jehc")
public class ApplicationRun {
	public static void main(String[] args) throws Exception{
        SpringApplication.run(ApplicationRun.class, args);
		ClusterTokenServer tokenServer = new SentinelDefaultTokenServer();
		tokenServer.start();
	}
}
