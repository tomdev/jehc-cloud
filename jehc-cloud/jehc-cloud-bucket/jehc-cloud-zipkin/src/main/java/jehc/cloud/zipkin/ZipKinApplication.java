package jehc.cloud.zipkin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import zipkin2.server.internal.EnableZipkinServer;
/**
 * @Desc 启动应用
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZipkinServer
public class ZipKinApplication {
	public static void main(String[] args)
	{

		SpringApplication.run(ZipKinApplication.class, args);
//		new SpringApplicationBuilder(ZipKinBoot.class)
//				.listeners(new RegisterZipkinHealthIndicators())
//				//.properties("spring.config.name=jehc-cloud-zipkin")
//				.run(args);
	}
}
