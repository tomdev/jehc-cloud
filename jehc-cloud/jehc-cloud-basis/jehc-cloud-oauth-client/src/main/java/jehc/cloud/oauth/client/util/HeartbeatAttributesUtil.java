package jehc.cloud.oauth.client.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
@Component
public class HeartbeatAttributesUtil {
    private boolean connected = true;//授权中心建立心跳
    private boolean ompCollencted = true;//OMP运管中心建立心跳
}
