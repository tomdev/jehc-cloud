package jehc.cloud.oauth.client.timer;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.oauth.client.util.HeartbeatAttributesUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Desc 建立心跳
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
public class HeartbeatScheduled {
    @Autowired
    RestTemplateUtil restTemplateUtil;

    @Autowired
    HeartbeatAttributesUtil heartbeatAttributesUtil;

    /**
     * 建立Token Server心跳
     */
    @Scheduled(cron="*/10 * * * * *")
    public void execute() {
        try {
            BaseResult baseResult =  restTemplateUtil.get(restTemplateUtil.restOauthURL()+"/heartbeat",BaseResult.class,null);
            if(null == baseResult || !baseResult.getSuccess()){
                log.error("无法连接授权中心...");
                heartbeatAttributesUtil.setConnected(false);
                return;
            }
            heartbeatAttributesUtil.setConnected(true);
            log.info("连接授权中心成功...");
        }catch (Exception e){
            heartbeatAttributesUtil.setConnected(false);
            log.error("连接授权中心异常：{}",e);
        }
    }

}
