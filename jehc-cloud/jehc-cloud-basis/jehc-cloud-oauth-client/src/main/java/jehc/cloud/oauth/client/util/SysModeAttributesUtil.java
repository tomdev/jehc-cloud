package jehc.cloud.oauth.client.util;

import cn.hutool.core.collection.CollectionUtil;
import jehc.cloud.common.util.StringUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Data
@Slf4j
public class SysModeAttributesUtil {

    private Map<String,String> attributes = new ConcurrentHashMap<>();
    /**
     * 存放
     * @param key
     * @param v
     * @return
     */
    public boolean put(String key,String v){
        boolean result = true;
        if(StringUtil.isEmpty(key)){
            log.info("The key is empty in SysModeAttributesUtil");
            return false;
        }
        if(CollectionUtil.isEmpty(attributes)){
            attributes = new ConcurrentHashMap<>();
        }
        try {
            attributes.put(key,v);
        }catch (Exception e){
            log.error("设置SysMode异常：{}",e);
            result = false;
        }
        return result;
    }

    /**
     * 删除
     * @param key
     * @return
     */
    public boolean remove(String key){
        boolean result = true;
        if(StringUtil.isEmpty(key)){
            log.info("The key is empty in SysModeAttributesUtil");
            return false;
        }
        try {
            attributes.remove(key);
        }catch (Exception e){
            log.error("移除SysMode异常：{}",e);
            result = false;
        }
        return result;
    }

    /**
     *
     * @param key
     * @return
     */
    public String get(String key){
        if(StringUtil.isEmpty(key)){
            log.info("The key is empty in SysModeAttributesUtil");
            return null;
        }
        return attributes.get(key);
    }
}
