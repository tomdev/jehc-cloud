package jehc.cloud.oauth.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.oauth.service.OauthSysModulesService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.dao.OauthSysModulesDao;
import jehc.cloud.oauth.model.OauthSysModules;
/**
 * @Desc 授权中心子系统模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthSysModulesService")
public class OauthSysModulesServiceImpl extends BaseService implements OauthSysModulesService {
	@Autowired
	private OauthSysModulesDao oauthSysModulesDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthSysModules> getOauthSysModulesListByCondition(Map<String,Object> condition){
		try{
			return oauthSysModulesDao.getOauthSysModulesListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param sys_modules_id 
	* @return
	*/
	public OauthSysModules getOauthSysModulesById(String sys_modules_id){
		try{
			OauthSysModules oauthSysModules = oauthSysModulesDao.getOauthSysModulesById(sys_modules_id);
			return oauthSysModules;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthSysModules 
	* @return
	*/
	public int addOauthSysModules(OauthSysModules oauthSysModules){
		int i = 0;
		try {
			oauthSysModules.setCreate_id(getXtUid());
			oauthSysModules.setCreate_time(getDate());
			i = oauthSysModulesDao.addOauthSysModules(oauthSysModules);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param oauthSysModules 
	* @return
	*/
	public int updateOauthSysModules(OauthSysModules oauthSysModules){
		int i = 0;
		try {
			oauthSysModules.setUpdate_id(getXtUid());
			oauthSysModules.setUpdate_time(getDate());
			i = oauthSysModulesDao.updateOauthSysModules(oauthSysModules);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param oauthSysModules 
	* @return
	*/
	public int updateOauthSysModulesBySelective(OauthSysModules oauthSysModules){
		int i = 0;
		try {
			oauthSysModules.setUpdate_id(getXtUid());
			oauthSysModules.setUpdate_time(getDate());
			i = oauthSysModulesDao.updateOauthSysModulesBySelective(oauthSysModules);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthSysModules(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthSysModulesDao.delOauthSysModules(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param oauthSysModulesList 
	* @return
	*/
	public int updateBatchOauthSysModules(List<OauthSysModules> oauthSysModulesList){
		int i = 0;
		try {
			i = oauthSysModulesDao.updateBatchOauthSysModules(oauthSysModulesList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param oauthSysModulesList 
	* @return
	*/
	public int updateBatchOauthSysModulesBySelective(List<OauthSysModules> oauthSysModulesList){
		int i = 0;
		try {
			i = oauthSysModulesDao.updateBatchOauthSysModulesBySelective(oauthSysModulesList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
