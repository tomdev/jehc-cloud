package jehc.cloud.oauth.service;

import jehc.cloud.common.base.BaseResult;

/**
 * @Desc 卸载模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthUninstallService {

    /**
     * 卸载模块数据
     * @param id
     * @return
     */
    BaseResult delForUninstall(String id);
}
