package jehc.cloud.oauth.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心子系统标记
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthSysMode extends BaseEntity {
	private String sys_mode_id;/****/
	private String sysmode;/**系统标记不可更改**/
	private String sysname;/**系统名称**/
	private int sys_mode_status;/**状态正常/禁用**/
	private String key_info_id;/**秘钥ID外键**/
	private int sort;/**排序编号**/
	private String sys_mode_icon;/**隶属平台图标**/
	private String sys_mode_url;/**平台地址路径**/
}
