package jehc.cloud.oauth.service;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.entity.UserParamInfo;
import jehc.cloud.common.entity.UserinfoEntity;
import jehc.cloud.oauth.model.OauthAccount;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
/**
 * @Desc 授权中心账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthAccountService {
    /**
     * 分页
     * @param condition
     * @return
     */
    List<OauthAccount> getOauthAccountList(Map<String,Object> condition);
    /**
     * 查询对象
     * @param xt_account_id
     * @return
     */
    OauthAccount getOauthAccountById(String xt_account_id);

    /**
     * 登录
     * @param condition
     * @return
     */
    OauthAccount login(Map<String,Object> condition);

    /**
     * 添加
     * @param oauthAccount
     * @return
     */
    int addOauthAccount(OauthAccount oauthAccount);
    /**
     * 修改（根据动态条件）
     * @param oauthAccount
     * @return
     */
    int updateOauthAccount(OauthAccount oauthAccount);

    /**
     * 冻结账户
     * @param oauthAccount
     * @return
     */
    int freezeAccount(OauthAccount oauthAccount);

    /**
     * 查询账户集合
     * @param condition
     * @return
     */
    List<OauthAccount> infoList(Map<String,Object> condition);

    /**
     * 修改登录时间
     * @param oauthAccount
     */
    void updateLoginTime(OauthAccount oauthAccount);

    /**
     * 修改密码
     * @param userParamInfo
     * @param request
     * @return
     */
    int updatePwd(UserParamInfo userParamInfo, HttpServletRequest request);

    /**
     * 同步数据
     * @param userinfoEntities
     * @return
     */
    BaseResult sync(List<UserinfoEntity> userinfoEntities);
}
