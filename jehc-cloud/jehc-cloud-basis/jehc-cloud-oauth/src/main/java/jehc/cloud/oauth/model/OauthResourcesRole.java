package jehc.cloud.oauth.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心资源对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthResourcesRole extends BaseEntity{
	private String resources_role_id;/****/
	private String role_id;/**角色ID外键**/
	private String resources_id;/**资源ID外键**/
}
