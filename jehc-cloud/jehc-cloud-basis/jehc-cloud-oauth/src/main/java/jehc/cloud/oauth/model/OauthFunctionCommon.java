package jehc.cloud.oauth.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心公共功能
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthFunctionCommon extends BaseEntity {
	private String function_common_id;/****/
	private String function_common_title;/**标题**/
	private String function_common_url;/**功能地址**/
	private String function_common_method;/**方法名称**/
	private String userName;/**创建人**/
	private int function_common_status;/**状态0启用1禁用**/
	private String function_common_content;/**备注**/
	private String sysmode_id;/**子系统id外键（即只有该系统下才能使用）**/
	private String sysname;
}
