package jehc.cloud.sys.init.task;

import jehc.cloud.common.cache.redis.RedisUtil;
import jehc.cloud.common.constant.CacheConstant;
import jehc.cloud.common.util.CollectionUtil;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.SpringUtils;
import jehc.cloud.sys.model.XtIpFrozen;
import java.util.*;
import java.util.concurrent.RecursiveTask;

/**
 * 多任务分解
 */
public class InitTask extends RecursiveTask<Integer> {


    private List<XtIpFrozen> ipFrozenList;

    public int groupNumber;

    public InitTask(List<XtIpFrozen> ipFrozenList,int groupNumber){
        this.ipFrozenList = ipFrozenList;
        this.groupNumber = groupNumber;
    }

    @Override
    protected Integer compute() {
        int result=0;
        if(ipFrozenList.size()<=groupNumber){
            //一个任务即可
            result+=doCallable(ipFrozenList);
        }else{
            //拆分任务
            List<InitTask> subTaskList =createSubTasks(ipFrozenList);
//            //方法一 采用fork（注意：该效率不是太高）
//            for(FasErrorTask subTask :subTaskList){
//                subTask.fork();//子任务计划执行
//            }

            //方法二 采用invokeAll（注意：该效率高一些）
            invokeAll(subTaskList);

            //合并执行结果
            for(InitTask subTask :subTaskList){
                result+=subTask.join();
            }
        }
        return result;
    }

    private Integer doCallable(List<XtIpFrozen> ipFrozenList){
        RedisUtil redisUtil = SpringUtils.getBean(RedisUtil.class);
        Integer result=0;
        for(XtIpFrozen ipFrozen: ipFrozenList){
            Boolean flag = redisUtil.hset(CacheConstant.SYSHASH,CacheConstant.XTIPFROZENCACHE+":"+ipFrozen.getAddress(), JsonUtil.toFastJson(ipFrozen));
            if(flag){
                result++;
            }
        }
       return result;
    }

    private List<InitTask> createSubTasks(List<XtIpFrozen> ipFrozenList) {
        List<InitTask> subTasks = new ArrayList<InitTask>();
        LinkedHashMap map = CollectionUtil.groupList(ipFrozenList,groupNumber);
        Iterator iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            InitTask initTask = new InitTask((List<XtIpFrozen>)entry.getValue(),groupNumber);
            subTasks.add(initTask);
        }
        return subTasks;
    }
}
