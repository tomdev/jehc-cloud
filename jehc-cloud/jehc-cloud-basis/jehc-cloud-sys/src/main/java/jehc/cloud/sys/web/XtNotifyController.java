package jehc.cloud.sys.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.sys.model.XtNotify;
import jehc.cloud.sys.model.XtNotifyReceiver;
import jehc.cloud.sys.service.XtNotifyService;
import jehc.cloud.sys.service.XtUserinfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
/**
 * @Desc 通知
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtNotify")
@Api(value = "通知API",tags = "通知API",description = "通知API")
public class XtNotifyController extends BaseAction {
	@Autowired
	private XtNotifyService xtNotifyService;
	@Autowired
	private XtUserinfoService xtUserinfoService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询通知列表并分页", notes="查询通知列表并分页")
	public BasePage getXtNotifyListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("xt_userinfo_id", getXtUid());
		commonHPager(baseSearch);
		List<XtNotify> xtNotifyList = xtNotifyService.getXtNotifyListByCondition(condition);
		PageInfo<XtNotify> page = new PageInfo<XtNotify>(xtNotifyList);
		return outPageBootStr(page,baseSearch);
	}
	
	
	/**
	* 添加
	* @param xtNotify
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个通知", notes="创建单个通知")
	public BaseResult addXtNotify(@RequestBody XtNotify xtNotify){
		int i = 0;
		if(null != xtNotify && !"".equals(xtNotify)){
			List<XtNotifyReceiver> xtNotifyReceiverList = new ArrayList<XtNotifyReceiver>();
			xtNotify.setXt_notify_id(toUUID());
			xtNotify.setXt_notify_ctime(getDate());
			String xt_userinfo_id = xtNotify.getXt_userinfo_id();
			if(!StringUtils.isEmpty(xt_userinfo_id)){
				String[] xt_userinfo_idList = xt_userinfo_id.split(",");
				for(int j = 0; j < xt_userinfo_idList.length; j++){
					XtNotifyReceiver xtNotifyReceiver = new XtNotifyReceiver();
					xtNotifyReceiver.setXt_notify_receiver_id(toUUID());
					xtNotifyReceiver.setXt_userinfo_id(xt_userinfo_idList[j]);
					xtNotifyReceiver.setXt_userinfo_realName(xtUserinfoService.getXtUserinfoById(xt_userinfo_idList[j]).getXt_userinfo_realName());
					xtNotifyReceiver.setReceive_time(xtNotify.getXt_notify_ctime());
					xtNotifyReceiverList.add(xtNotifyReceiver);
				}
				xtNotify.setXtNotifyReceiverList(xtNotifyReceiverList);
			}
			xtNotify.setXt_userinfo_id(getXtUid());
			xtNotify.setXt_userinfo_realName(getXtU().getXt_userinfo_realName());
			i=xtNotifyService.addXtNotify(xtNotify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_notify_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除单个通知", notes="删除单个通知")
	public BaseResult delXtNotify(String xt_notify_id){
		int i = 0;
		if(null != xt_notify_id && !"".equals(xt_notify_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_notify_id",xt_notify_id.split(","));
			i=xtNotifyService.delXtNotify(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
}
