package jehc.cloud.sys.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import jehc.cloud.common.base.BaseEntity;

/**
 * @Desc 行政区划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class XtAreaRegion extends BaseEntity{
	@JsonProperty(value = "ID")
	private String ID;/**ID编号**/
	@JsonProperty(value = "PARENT_ID")
	private int PARENT_ID;/**父级ID**/
	@JsonProperty(value = "CODE")
	private String CODE;/**政行编码**/
	@JsonProperty(value = "NAME")
	private String NAME;/**名称**/
	@JsonProperty(value = "REGION_LEVEL")
	private int REGION_LEVEL;/**行政级别**/
	@JsonProperty(value = "NAME_EN")
	private String NAME_EN;/**英文**/
	@JsonProperty(value = "LONGITUDE")
	private double LONGITUDE;/**经度**/
	@JsonProperty(value = "LATITUDE")
	private double LATITUDE;/**纬度**/
	public void setID(String ID){
		this.ID=ID;
	}
	public String getID(){
		return ID;
	}
	public void setPARENT_ID(int PARENT_ID){
		this.PARENT_ID=PARENT_ID;
	}
	public int getPARENT_ID(){
		return PARENT_ID;
	}
	public void setCODE(String CODE){
		this.CODE=CODE;
	}
	public String getCODE(){
		return CODE;
	}
	public void setNAME(String NAME){
		this.NAME=NAME;
	}
	public String getNAME(){
		return NAME;
	}
	public void setREGION_LEVEL(int REGION_LEVEL){
		this.REGION_LEVEL=REGION_LEVEL;
	}
	public int getREGION_LEVEL(){
		return REGION_LEVEL;
	}
	public void setNAME_EN(String NAME_EN){
		this.NAME_EN=NAME_EN;
	}
	public String getNAME_EN(){
		return NAME_EN;
	}
	public void setLONGITUDE(double LONGITUDE){
		this.LONGITUDE=LONGITUDE;
	}
	public double getLONGITUDE(){
		return LONGITUDE;
	}
	public void setLATITUDE(double LATITUDE){
		this.LATITUDE=LATITUDE;
	}
	public double getLATITUDE(){
		return LATITUDE;
	}
}
