package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Desc 通知接收人
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtNotifyReceiver extends BaseEntity{
	private String xt_notify_receiver_id;/**id**/
	private Date receive_time;/**接收时间**/ 
	private String xt_userinfo_id;/**接收人编号**/
	private String xt_userinfo_realName;/**接收人名称**/
	private String xt_notify_id;/**通知编号外键**/
	private int isDelete;/**是否删除0正常 1已删除**/
	private Date read_time;/**已读时间**/
	private int receive_status;/**状态0未读1已读**/
	
	//通知表信息
	private String sendUserRealName;/**发送人名称**/
	private String xt_notify_title;
	private String xt_notify_content;
}
