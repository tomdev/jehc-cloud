package jehc.cloud.sys.service.impl;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import jehc.cloud.log.client.service.LogsUtil;
import jehc.cloud.sys.dao.XtDepartinfoDao;
import jehc.cloud.sys.model.XtDepartinfo;
import jehc.cloud.sys.service.XtDepartinfoService;
import jehc.cloud.sys.service.XtPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 部门信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtDepartinfoService")
public class XtDepartinfoServiceImpl extends BaseService implements XtDepartinfoService {
	@Autowired
	XtDepartinfoDao xtDepartinfoDao;

	@Autowired
	XtPostService xtPostService;

	@Autowired
	LogsUtil logsUtil;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtDepartinfo> getXtDepartinfoListByCondition(Map<String,Object> condition){
		try {
			return xtDepartinfoDao.getXtDepartinfoListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_departinfo_id 
	* @return
	*/
	public XtDepartinfo getXtDepartinfoById(String xt_departinfo_id){
		try {
			return xtDepartinfoDao.getXtDepartinfoById(xt_departinfo_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtDepartinfo
	* @return
	*/
	public int addXtDepartinfo(XtDepartinfo xtDepartinfo){
		int i = 0;
		try {
			xtDepartinfo.setCreate_id(getXtUid());
			xtDepartinfo.setCreate_time(getDate());
			i = xtDepartinfoDao.addXtDepartinfo(xtDepartinfo);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("部门业务类", "添加", "执行添加部门成功");
		} catch (Exception e) {
			i = 0;
			logsUtil.aBLogs("部门业务类", "添加", "执行修改部门失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtDepartinfo
	* @return
	*/
	public int updateXtDepartinfo(XtDepartinfo xtDepartinfo){
		int i = 0;
		try {
			xtDepartinfo.setUpdate_id(getXtUid());
			xtDepartinfo.setUpdate_time(getDate());
			i = xtDepartinfoDao.updateXtDepartinfo(xtDepartinfo);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("部门业务类", "修改", "执行修改部门成功");
		} catch (Exception e) {
			i = 0;
			logsUtil.aBLogs("部门业务类", "修改", "执行修改部门失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtDepartinfo(Map<String,Object> condition){
		int i = 0;
		try {
			String[] xt_departinfo_idList = (String[])condition.get("xt_departinfo_id");
			for(String xt_departinfo_id:xt_departinfo_idList){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("del_flag", 0);
				map.put("xt_departinfo_parentId", xt_departinfo_id);
				//验证下级部门是否存在（正常部门）
				if(!xtDepartinfoDao.getXtDepartinfoListChild(map).isEmpty()){
					logsUtil.aBLogs("部门业务类", "删除", "执行删除部门，编号【"+xt_departinfo_id+"】存在下级部门，不能删除");
					return 0;
				}
				//验证是否存在岗位（正常岗位）
				map = new HashMap<String, Object>();
				map.put("del_flag", 0);
				map.put("xt_departinfo_id", xt_departinfo_id);
				if(!xtPostService.getXtPostinfoList(map).isEmpty()){
					logsUtil.aBLogs("部门业务类", "删除", "执行删除部门，编号【"+xt_departinfo_id+"】存在被岗位使用中，不能删除");
					return 0;
				}
			}
			i = xtDepartinfoDao.delXtDepartinfo(condition);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("部门业务类", "删除", "执行删除部门成功");
		} catch (Exception e) {
			logsUtil.aBLogs("部门业务类", "删除", "执行删除部门失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 部门根目录集合
	 * @return
	 */
	public List<XtDepartinfo> getXtDepartinfoList(){
		try {
			return xtDepartinfoDao.getXtDepartinfoList();
		} catch (Exception e) {
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 查找子集合
	 * @param condition
	 * @return
	 */
	public List<XtDepartinfo> getXtDepartinfoListChild(Map<String,Object> condition){
		try {
			return xtDepartinfoDao.getXtDepartinfoListChild(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 查找所有集合
	 * @param condition
	 * @return
	 */
	public List<XtDepartinfo> getXtDepartinfoListAll(Map<String,Object> condition){
		try {
			return xtDepartinfoDao.getXtDepartinfoListAll(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理组 发起组等使用）
	 * @param condition
	 * @return
	 */
	public List<XtDepartinfo> queryXtDepartinfoList(Map<String,Object> condition){
		return xtDepartinfoDao.queryXtDepartinfoList(condition);
	}
}
