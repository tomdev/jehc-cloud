package jehc.cloud.sys.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.sys.model.XtNotifyReceiver;
import jehc.cloud.sys.service.XtNotifyReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
/**
 * @Desc 通知接收人
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtNotifyReceiver")
@Api(value = "通知接收人API",tags = "通知接收人API",description = "通知接收人API")
public class XtNotifyReceiverController extends BaseAction {
	@Autowired
	private XtNotifyReceiverService xtNotifyReceiverService;
	
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	* @param receive_status
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询通知接收人列表并分页", notes="查询通知接收人列表并分页")
	public BasePage getXtNotifyReceiverListByCondition(@RequestBody(required=true)BaseSearch baseSearch, String receive_status){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("xt_userinfo_id", getXtUid());
		condition.put("receive_status",receive_status);
		commonHPager(baseSearch);
		List<XtNotifyReceiver> xtNotifyReceiverList = xtNotifyReceiverService.getXtNotifyReceiverListByCondition(condition);
		PageInfo<XtNotifyReceiver> page = new PageInfo<XtNotifyReceiver>(xtNotifyReceiverList);
		return outPageBootStr(page,baseSearch);
	}

	
	/**
	* 删除
	* @param xt_notify_receiver_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除通知接收人", notes="删除通知接收人")
	public BaseResult delXtNotifyReceiver(String xt_notify_receiver_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_notify_receiver_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_notify_receiver_id",xt_notify_receiver_id.split(","));
			i=xtNotifyReceiverService.delXtNotifyReceiver(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
