package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtNoticeDao;
import jehc.cloud.sys.model.XtNotice;
import jehc.cloud.sys.service.XtNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

/**
 * @Desc 平台公告
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtNoticeService")
public class XtNoticeServiceImpl extends BaseService implements XtNoticeService {
	@Autowired
	private XtNoticeDao xtNoticeDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	@SuppressWarnings("unchecked")
	public List<XtNotice> getXtNoticeListByCondition(Map<String,Object> condition){
		try{
			return xtNoticeDao.getXtNoticeListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_notice_id 
	* @return
	*/
	public XtNotice getXtNoticeById(String xt_notice_id){
		try{
			return xtNoticeDao.getXtNoticeById(xt_notice_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtNotice
	* @return
	*/
	public int addXtNotice(XtNotice xtNotice){
		int i = 0;
		try {
			i = xtNoticeDao.addXtNotice(xtNotice);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtNotice
	* @return
	*/
	public int updateXtNotice(XtNotice xtNotice){
		int i = 0;
		try {
			i = xtNoticeDao.updateXtNotice(xtNotice);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtNotice(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtNoticeDao.delXtNotice(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 统计
	 * @param condition
	 * @return
	 */
	public int getXtNoticeCountByCondition(Map<String,Object> condition){
		try{
			return xtNoticeDao.getXtNoticeCountByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
