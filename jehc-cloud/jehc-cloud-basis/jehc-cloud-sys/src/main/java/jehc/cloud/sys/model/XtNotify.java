package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Desc 通知
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtNotify  extends BaseEntity{
	private String xt_notify_id;/**通知编号**/
	private String xt_notify_title;/**通知标题**/
	private String xt_notify_content;/**通知内容**/
	private Date xt_notify_ctime;/**创建时间**/
	private String xt_userinfo_id;/**创建人编号**/
	private String xt_userinfo_realName;/**创建人名称**/
	private int isDelete;/**是否删除0正常1已删除**/
	private int xt_notify_type;/**通知类型0默认1平台通知(系统自动通知）**/
	private List<XtNotifyReceiver> xtNotifyReceiverList;
}
