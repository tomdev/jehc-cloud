package jehc.cloud.sys.web;

import com.github.pagehelper.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.*;
import jehc.cloud.common.entity.*;
import jehc.cloud.common.util.FastJsonUtils;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLDecoder;
import java.util.*;


/**
 * @Desc 通用
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtCommon")
@Api(value = "通用API",tags = "通用API",description = "通用API")
public class XtCommonController extends BaseAction {
	@Autowired
	RestTemplateUtil restTemplateUtil;
	@Autowired
	BaseUtils baseUtils;
	@Autowired
	CommonUtils commonUtils;
	
	/**
	 * 组织机构树列表
	 * @param id
	 * @param type
	 * @param request
	 * @throws UnsupportedEncodingException 
	 */
	@GetMapping(value="/get/xtOrgTree/{id}/{type}")
	@ApiOperation(value="查询组织机构树列表", notes="查询组织机构树列表")
	@NeedLoginUnAuth
	public BaseResult getXtOrgTree(@PathVariable("id")String id, @PathVariable("type")String type, HttpServletRequest request) throws UnsupportedEncodingException{
		type = URLDecoder.decode(type, "UTF-8");
		StringBuffer jsonStr = new StringBuffer("");  
		if(null != id && !"".equals(id) && "0".equals(id)){
			jsonStr.append("[");
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtDepartinfo/one/level/lists",BaseResult.class, request);
			List<DepartinfoEntity> xtDepartinfoList = JsonUtil.toFastList(baseResult.getData(),DepartinfoEntity.class);
			for(int i = 0; i < xtDepartinfoList.size(); i++){
				DepartinfoEntity xtDepartinfo = xtDepartinfoList.get(i);
				if(i==(xtDepartinfoList.size()-1)){
					jsonStr.append("{id:'"+xtDepartinfo.getXt_departinfo_id()+"',text:'"+xtDepartinfo.getXt_departinfo_name()+"',icon:'../deng/images/icons/depart.png',xt_departinfo_parentId:'"+xtDepartinfo.getXt_departinfo_parentId()+"',type:'部门',remark:'"+xtDepartinfo.getXt_departinfo_desc()+"'}");  
				}else{
					jsonStr.append("{id:'"+xtDepartinfo.getXt_departinfo_id()+"',text:'"+xtDepartinfo.getXt_departinfo_name()+"',icon:'../deng/images/icons/depart.png',xt_departinfo_parentId:'"+xtDepartinfo.getXt_departinfo_parentId()+"',type:'部门',remark:'"+xtDepartinfo.getXt_departinfo_desc()+"'},");
				}
			}
			jsonStr.append("]");
		}else{
			//1查找部门
			jsonStr.append("[");
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_departinfo_parentId", id);
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtDepartinfo/child/lists/"+id,BaseResult.class, request);
			List<DepartinfoEntity> xtDepartinfoList =  JsonUtil.toFastList(baseResult.getData(),DepartinfoEntity.class);
			for(int i = 0; i < xtDepartinfoList.size(); i++){
				DepartinfoEntity xtDepartinfo = xtDepartinfoList.get(i);
				String leaf = "false";
				if(i==(xtDepartinfoList.size()-1)){
					if(xtDepartinfo.getXt_departinfo_leaf() == 0){
						leaf = "false";
					}else{
						leaf = "true";
					}
					jsonStr.append("{id:'"+xtDepartinfo.getXt_departinfo_id()+"',text:'"+xtDepartinfo.getXt_departinfo_name()+"',leaf:"+leaf+",icon:'../deng/images/icons/depart.png',xt_departinfo_parentId:'"+xtDepartinfo.getXt_departinfo_parentId()+"',type:'部门',remark:'"+xtDepartinfo.getXt_departinfo_desc()+"'}");  
				}else{
					if(xtDepartinfo.getXt_departinfo_leaf() == 0){
						leaf = "false";
					}else{
						leaf = "true";
					}
					jsonStr.append("{id:'"+xtDepartinfo.getXt_departinfo_id()+"',text:'"+xtDepartinfo.getXt_departinfo_name()+"',leaf:"+leaf+",icon:'../deng/images/icons/depart.png',xt_departinfo_parentId:'"+xtDepartinfo.getXt_departinfo_parentId()+"',type:'部门',remark:'"+xtDepartinfo.getXt_departinfo_desc()+"'},");  
				}
			}
			//2查找部门下面一级岗位
			String xtPostStr = xtPostStr(id,type,request);
			if(null != xtPostStr && !"".equals(xtPostStr) && !"[".equals(jsonStr.toString())){
				jsonStr.append(","+xtPostStr);
			}else{
				jsonStr.append(xtPostStr);
			}
			jsonStr.append("]");
		}
		return outStr(jsonStr.toString());
	}
	
	/**
	 * 返回岗位字符串
	 * @param id
	 * @return
	 */
	public String xtPostStr(String id,String type, HttpServletRequest request){
		//如果类型:部门 则查出所有部门下的一级岗位
		StringBuffer jsonStr = new StringBuffer(); 
		Map<String, Object> condition = new HashMap<String, Object>();
		if(null != type && !"".equals(type) && "部门".equals(type)){
			condition.put("xt_departinfo_id", id);
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtPost/one/level/lists/"+id,BaseResult.class, request);

			List<PostEntity> xtPostList = JsonUtil.toFastList(baseResult.getData(),PostEntity.class);
			//拼接字符串
			for(int i = 0; i < xtPostList.size(); i++){
				PostEntity xtPost = xtPostList.get(i);
				if(i==(xtPostList.size()-1)){
					jsonStr.append("{id:'"+xtPost.getXt_post_id()+"',text:'"+xtPost.getXt_post_name()+"',icon:'../deng/images/icons/users.png',xt_post_parentId:'"+xtPost.getXt_post_parentId()+"',type:'岗位',remark:'"+xtPost.getXt_post_desc()+"',xt_departinfo_id:'"+xtPost.getXt_departinfo_id()+"',xt_departinfo_name:'"+xtPost.getXt_departinfo_name()+"'}");  
				}else{
					jsonStr.append("{id:'"+xtPost.getXt_post_id()+"',text:'"+xtPost.getXt_post_name()+"',icon:'../deng/images/icons/users.png',xt_post_parentId:'"+xtPost.getXt_post_parentId()+"',type:'岗位',remark:'"+xtPost.getXt_post_desc()+"',xt_departinfo_id:'"+xtPost.getXt_departinfo_id()+"',xt_departinfo_name:'"+xtPost.getXt_departinfo_name()+"'},");
				}
			}
		}
		return jsonStr.toString();
	}
	


	
	/**
	 * 查询字典集合（根据ckey）
	 * @param ckey
	 * @return
	 */
	@AuthUneedLogin
	@GetMapping(value="/xtDataDictionary/list/{ckey}")
	@ApiOperation(value="获取字典集合（根据ckey）", notes="获取字典集合（根据ckey）")
	public BaseResult getXtDataDictionaryList(@PathVariable("ckey")String ckey){
		return outItemsStr(commonUtils.getXtDataDictionaryCache(ckey));
	}

	/**
	 * 全部路径
	 * @return
	 */
	@GetMapping(value="/path/list")
	@AuthUneedLogin
	@ApiOperation(value="查询全部路径", notes="查询全部路径")
	public BaseResult pathList(String key){
		List<PathEntity> pathList = commonUtils.getXtPathCache(key);
		return new BaseResult(FastJsonUtils.toJSONString(pathList));
	}

	/**
	 * 读取所有省份
	 * @return
	 */
	@GetMapping(value="/pList")
	@AuthUneedLogin
	@ApiOperation(value="查询全部省份", notes="查询全部省份")
	public BaseResult getPList(){
		List<AreaRegionEntity> list = commonUtils.getXtAreaRegionCache(null);
		return outItemsStr(list);
	}

	/**
	 * 读取城市
	 * @return
	 */
	@GetMapping(value="/cList/{parentId}")
	@AuthUneedLogin
	@ApiOperation(value="查询全部城市", notes="查询全部城市")
	public BaseResult getCList(@PathVariable("parentId") String parentId){
		List<AreaRegionEntity> list = new ArrayList<AreaRegionEntity>();
		if(StringUtil.isEmpty(parentId)){
			return outItemsStr(list);
		}
		list = commonUtils.getXtAreaRegionCache(parentId);
		return outItemsStr(list);
	}

	/**
	 * 读取区县
	 * @return
	 */
	@GetMapping(value="/dList/{parentId}")
	@AuthUneedLogin
	@ApiOperation(value="查询全部区县", notes="查询全部区县")
	public BaseResult getDList(@PathVariable("parentId") String parentId){
		List<AreaRegionEntity> list = new ArrayList<AreaRegionEntity>();
		if(StringUtil.isEmpty(parentId)){
			return outItemsStr(list);
		}
		list = commonUtils.getXtAreaRegionCache(parentId);
		return outItemsStr(list);
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	@GetMapping(value="/constantByKey/{key}")
	@AuthUneedLogin
	@ApiOperation(value="根据Key查询常量", notes="根据Key查询常量")
	public ConstantEntity getConstantCache(@PathVariable("key") String key){
		return commonUtils.getXtConstantCache(key);
	}

	/**
	 *
	 * @param xt_constant_id
	 * @return
	 */
	@GetMapping(value="/constant/{xt_constant_id}")
	@AuthUneedLogin
	@ApiOperation(value="根据Id查询常量", notes="根据Id查询常量")
	public ConstantEntity getConstant(@PathVariable("xt_constant_id") String xt_constant_id,HttpServletRequest request){
		if(StringUtil.isEmpty(xt_constant_id)){
			return new ConstantEntity();
		}
		ConstantEntity constantEntity = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtConstant/get/"+xt_constant_id,ConstantEntity.class,request);
		return constantEntity;
	}

	/**
	 *
	 * @param quartzLog
	 * @param request
	 * @return
	 */
	@PostMapping(value="/xtQuartzLog/add")
	@AuthUneedLogin
	@ApiOperation(value="添加定时任务日志", notes="添加定时任务日志")
	public BaseResult getConstant(QuartzLogEntity quartzLog,HttpServletRequest request){
		if(null == quartzLog){
			return new BaseResult();
		}
		BaseResult baseResult = restTemplateUtil.post(restTemplateUtil.restSysURL() + "/xtQuartzLog/add",BaseResult.class,quartzLog,request);
		return baseResult;
	}

	/**
	 *
	 * @param dbEntity
	 * @param request
	 * @return
	 */
	@PostMapping(value="/xtDb/add")
	@AuthUneedLogin
	@ApiOperation(value="创建数据库信息", notes="创建数据库信息")
	public BaseResult addDb(DbEntity dbEntity,HttpServletRequest request){
		if(null == dbEntity){
			return new BaseResult();
		}
		BaseResult baseResult = restTemplateUtil.post(restTemplateUtil.restSysURL() + "/xtdb/add",BaseResult.class,dbEntity,request);
		return baseResult;
	}


	/**
	 * 获取全部公共功能 通过缓存获取
	 * @param request
	 * @return
	 */
	@GetMapping(value="/functioninfoCommon/list")
	@AuthUneedLogin
	@ApiOperation(value="查询全部功能", notes="查询全部功能")
	public String getConstant(HttpServletRequest request){
		return commonUtils.getXtFunctioninfoCommonCache();
	}

	/**
	 * 判断IP是否为黑户
	 * @param ip
	 * @return
	 */
	@GetMapping(value="/ipFrozen/validate/{ip}")
	@AuthUneedLogin
	@ApiOperation(value="判断IP是否为黑户", notes="判断IP是否为黑户")
	public boolean validateIp(@PathVariable("ip") String ip){
		return commonUtils.getXtIpFrozenCache(ip);
	}

	/**
	 * 根据KEY获取平台字典
	 * @param key
	 * @return
	 */
	@GetMapping(value="/dataDictionary/list/{key}")
	@AuthUneedLogin
	@ApiOperation(value="根据KEY获取平台字典", notes="根据KEY获取平台字典")
	public List<DataDictionaryEntity> getXtDataDictionaryCache(@PathVariable("key") String key) {
		return commonUtils.getXtDataDictionaryCache(key);
	}

	/**
	 * 根据KEY获取平台路径
	 * @param key
	 * @return
	 */
	@GetMapping(value="path/list/{key}")
	@AuthUneedLogin
	@ApiOperation(value="根据KEY获取平台路径", notes="根据KEY获取平台路径")
	public List<PathEntity> getXtPathCache(@PathVariable("key") String key) {
		return commonUtils.getXtPathCache(key);
	}
}
