package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 商品(产品)单位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtUnit extends BaseEntity{
	private String xt_unit_id;/**单位编号**/
	private String name;/**单位名称**/
}
