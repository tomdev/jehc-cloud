package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtPathDao;
import jehc.cloud.sys.model.XtPath;
import jehc.cloud.sys.service.XtPathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

/**
 * @Desc 文件路径
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtPathService")
public class XtPathServiceImpl extends BaseService implements XtPathService {
	@Autowired
	private XtPathDao xtPathDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	@SuppressWarnings("unchecked")
	public List<XtPath> getXtPathListByCondition(Map<String,Object> condition){
		try {
			return xtPathDao.getXtPathListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_path_id 
	* @return
	*/
	public XtPath getXtPathById(String xt_path_id){
		try {
			return xtPathDao.getXtPathById(xt_path_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtPath
	* @return
	*/
	public int addXtPath(XtPath xtPath){
		int i = 0;
		try {
			xtPath.setCreate_id(getXtUid());
			xtPath.setCreate_time(getDate());
			i = xtPathDao.addXtPath(xtPath);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtPath
	* @return
	*/
	public int updateXtPath(XtPath xtPath){
		int i = 0;
		try {
			xtPath.setUpdate_id(getXtUid());
			xtPath.setUpdate_time(getDate());
			i = xtPathDao.updateXtPath(xtPath);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtPath(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtPathDao.delXtPath(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 查找所有平台路径
	 * @param condition
	 * @return
	 */
	public List<XtPath> getXtPathListAllByCondition(Map<String,Object> condition){
		try {
			return xtPathDao.getXtPathListAllByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
