package jehc.cloud.log.service.impl;
import java.util.List;
import java.util.Map;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.log.service.LogLoginService;
import jehc.cloud.log.dao.LogLoginDao;
import jehc.cloud.log.model.LogLogin;

/**
* @Desc 登录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:21:01
*/
@Service("logLoginService")
public class LogLoginServiceImpl extends BaseService implements LogLoginService{
	@Autowired
	private LogLoginDao logLoginDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LogLogin> getLogLoginListByCondition(Map<String,Object> condition){
		try{
			return logLoginDao.getLogLoginListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LogLogin getLogLoginById(String id){
		try{
			LogLogin logLogin = logLoginDao.getLogLoginById(id);
			return logLogin;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param logLogin 
	* @return
	*/
	public int addLogLogin(LogLogin logLogin){
		int i = 0;
		try {
			i = logLoginDao.addLogLogin(logLogin);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param logLogin 
	* @return
	*/
	public int updateLogLogin(LogLogin logLogin){
		int i = 0;
		try {
			i = logLoginDao.updateLogLogin(logLogin);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param logLogin 
	* @return
	*/
	public int updateLogLoginBySelective(LogLogin logLogin){
		int i = 0;
		try {
			i = logLoginDao.updateLogLoginBySelective(logLogin);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLogLogin(Map<String,Object> condition){
		int i = 0;
		try {
			i = logLoginDao.delLogLogin(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
