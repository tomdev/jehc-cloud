package jehc.cloud.log.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;

/**
* @Desc 页面加载信息 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:22:05
*/
@Data
public class LogLoadinfo extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String modules;/**载加模块**/
	private Long begtime;/**载入时间**/
	private Long endtime;/**载入结束时间**/
}
