package jehc.cloud.job.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.job.model.JobConfig;
import jehc.cloud.job.service.JobConfigService;
import jehc.cloud.job.dao.JobConfigDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

/**
* 任务调度配置信息表 
* 2015-10-29 16:50:03  邓纯杰
*/
@Service
public class JobConfigServiceImpl extends BaseService implements JobConfigService {
	@Autowired
	private JobConfigDao jobConfigDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<JobConfig> getJobConfigListByCondition(Map<String,Object> condition){
		try{
			return jobConfigDao.getJobConfigListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public JobConfig getJobConfigById(String id){
		try{
			return jobConfigDao.getJobConfigById(id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param jobConfig
	* @return
	*/
	public int addJobConfig(JobConfig jobConfig){
		int i = 0;
		try {
			i = jobConfigDao.addJobConfig(jobConfig);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param jobConfig
	* @return
	*/
	public int updateJobConfig(JobConfig jobConfig){
		int i = 0;
		try {
			i = jobConfigDao.updateJobConfig(jobConfig);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delJobConfig(Map<String,Object> condition){
		int i = 0;
		try {
			i = jobConfigDao.delJobConfig(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 查找集合
	 * @param condition
	 * @return
	 */
	public List<JobConfig> getJobConfigListAllByCondition(Map<String,Object> condition){
		try{
			return jobConfigDao.getJobConfigListAllByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
