package jehc.cloud.job.config;

import jehc.cloud.common.util.logger.Logback4jUtil;
import jehc.cloud.job.command.InitScheduler;
import jehc.cloud.oauth.client.AuthHandler;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ConfigBean extends Logback4jUtil implements WebMvcConfigurer {

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /**
     * 由于在拦截器中注解无效，需要提前注入bean
     * @return
     */
    @Bean
    public AuthHandler authHandler(){
        return new AuthHandler();
    }

    /**
     * 放开资源
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(authHandler());
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/favicon.ico",
                "/js/**",
                "/html/**",
                "/resources/*",
                "/webjars/**",
                "/v2/api-docs/**",
                "/swagger-ui.html/**",
                "/swagger-resources/**",
                "/oauth",
                "/isAdmin",
                "/accountInfo",
                "/httpSessionEntity"
        );
    }
}
