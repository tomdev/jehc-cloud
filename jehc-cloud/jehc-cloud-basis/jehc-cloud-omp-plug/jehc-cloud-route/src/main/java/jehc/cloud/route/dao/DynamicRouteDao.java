package jehc.cloud.route.dao;
import jehc.cloud.route.model.DynamicRoute;

import java.util.List;
import java.util.Map;

/**
 * @Desc 路由信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface DynamicRouteDao {

	/**
	* 查询列表
	* @param condition
	* @return
	*/
	List<DynamicRoute> getDynamicRouteListByCondition(Map<String, Object> condition);

	/**
	* 查询对象
	* @param id
	* @return
	*/
	DynamicRoute getDynamicRouteById(String id);

	/**
	* 添加
	* @param dynamicRoute
	* @return
	*/
	int addDynamicRoute(DynamicRoute dynamicRoute);

	/**
	* 修改
	* @param dynamicRoute
	* @return
	*/
	int updateDynamicRoute(DynamicRoute dynamicRoute);

	/**
	 * 更新状态
	 * @param dynamicRoute
	 * @return
	 */
	int updateStatus(DynamicRoute dynamicRoute);

	/**
	* 删除
	* @param condition
	* @return
	*/
	int delDynamicRoute(Map<String, Object> condition);

	/**
	 * 查询列表（供缓存使用）
	 * @param condition
	 * @return
	 */
	List<DynamicRoute> getDynamicRouteList(Map<String, Object> condition);
}
