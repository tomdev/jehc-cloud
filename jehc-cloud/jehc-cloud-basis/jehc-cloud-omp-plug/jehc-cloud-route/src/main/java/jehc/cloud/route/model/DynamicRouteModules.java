package jehc.cloud.route.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 路由模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@ApiModel(value = "路由模块")
@Data
public class DynamicRouteModules extends BaseEntity {
    @ApiModelProperty(value = "主键")
    private String id;/**主键**/

    @ApiModelProperty(value = "名称")
    private String name;/**名称**/
}
