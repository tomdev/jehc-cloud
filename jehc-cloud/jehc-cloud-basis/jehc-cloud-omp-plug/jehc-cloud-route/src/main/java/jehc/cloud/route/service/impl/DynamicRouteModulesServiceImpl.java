package jehc.cloud.route.service.impl;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.route.dao.DynamicRouteModulesDao;
import jehc.cloud.route.model.DynamicRouteModules;
import jehc.cloud.route.service.DynamicRouteModulesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 路由模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class DynamicRouteModulesServiceImpl extends BaseService implements DynamicRouteModulesService {

    @Autowired
    private DynamicRouteModulesDao dynamicRouteModulesDao;


    /**
     * 查询列表
     * @param condition
     * @return
     */
    public List<DynamicRouteModules> getDynamicRouteModulesListByCondition(Map<String, Object> condition){
        return dynamicRouteModulesDao.getDynamicRouteModulesListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public DynamicRouteModules getDynamicRouteModulesById(String id){
        return dynamicRouteModulesDao.getDynamicRouteModulesById(id);
    }

    /**
     * 添加
     * @param dynamicRouteModules
     * @return
     */
    public int addDynamicRouteModules(DynamicRouteModules dynamicRouteModules){
        dynamicRouteModules.setId(toUUID());
        dynamicRouteModules.setCreate_time(getDate());
        dynamicRouteModules.setCreate_id(getXtUid());
        return dynamicRouteModulesDao.addDynamicRouteModules(dynamicRouteModules);
    }

    /**
     * 修改
     * @param dynamicRouteModules
     * @return
     */
    public int updateDynamicRouteModules(DynamicRouteModules dynamicRouteModules){
        dynamicRouteModules.setUpdate_time(getDate());
        dynamicRouteModules.setUpdate_id(getXtUid());
        return dynamicRouteModulesDao.updateDynamicRouteModules(dynamicRouteModules);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delDynamicRouteModules(Map<String, Object> condition){
        return dynamicRouteModulesDao.delDynamicRouteModules(condition);
    }
}
