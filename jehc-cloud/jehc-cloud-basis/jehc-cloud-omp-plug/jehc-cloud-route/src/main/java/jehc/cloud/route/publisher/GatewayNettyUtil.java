package jehc.cloud.route.publisher;

import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.util.CharsetUtil;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.route.vo.ChannelEntity;
import jehc.cloud.route.vo.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Desc 网关路由Netty 工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Component
public class GatewayNettyUtil {

    /**
     * 发送消息
     * @param channelEntity
     */
    public boolean sendMessage(ChannelEntity channelEntity){
        boolean result = true;
        try {
            if(null == channelEntity.getChannel()){
                log.info("未能获取到网关路由通道号");
                result = false;
            }else{
                RequestInfo requestInfo = channelEntity.getRequestInfo();
                String json = JSONObject.toJSONString(requestInfo);
                //创建一个持有数据的ByteBuf
                ByteBuf buf = Unpooled.copiedBuffer(json, CharsetUtil.UTF_8);
                //数据冲刷
                ChannelFuture cf = channelEntity.getChannel().writeAndFlush(buf);
                //添加ChannelFutureListener以便在写操作完成后接收通知
                cf.addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture future) throws Exception {
                        if (future.isSuccess()){
                            log.info("路由服务发送消息至网关路成功...");
                        }else{
                            log.info("路由服务发送消息至网关路失败...");
                            future.cause().printStackTrace();
                        }
                    }
                });
            }
        }catch (Exception e){
            log.error("网关路由发送消息异常：",e);
            result =  false;
        }
        return result;
    }
}
