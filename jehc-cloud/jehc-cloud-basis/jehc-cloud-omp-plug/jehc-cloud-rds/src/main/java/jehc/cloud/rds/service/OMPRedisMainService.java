package jehc.cloud.rds.service;

import jehc.cloud.rds.model.OMPRedisMain;

import java.util.List;
import java.util.Map;

/**
 * @Desc Redis主监控信息日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OMPRedisMainService {
    /**
     * 初始化分页
     * @param condition
     * @return
     */
    List<OMPRedisMain> getRedisMainListByCondition(Map<String, Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    OMPRedisMain getRedisMainById(String id);

    /**
     * 添加
     * @param ompRedisMain
     * @return
     */
    int addRedisMain(OMPRedisMain ompRedisMain);

    /**
     * 删除
     * @param map
     * @return
     */
    int delRedisMain(Map<String, Object> map);

    /**
     * 查询列表
     * @param ompRedisMain
     * @return
     */
    List<OMPRedisMain> getRedisMainList(OMPRedisMain ompRedisMain);
}
