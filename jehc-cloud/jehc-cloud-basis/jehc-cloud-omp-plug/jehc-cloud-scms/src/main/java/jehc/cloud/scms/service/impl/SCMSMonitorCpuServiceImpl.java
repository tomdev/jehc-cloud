package jehc.cloud.scms.service.impl;

import jehc.cloud.scms.service.SCMSMonitorCpuService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.scms.dao.SCMSMonitorCpuDao;
import jehc.cloud.scms.model.SCMSMonitorCpu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 服务器CPU运行
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class SCMSMonitorCpuServiceImpl extends BaseService implements SCMSMonitorCpuService {
    @Autowired
    SCMSMonitorCpuDao scmsMonitorCpuDao;

    /**
     * 查询监控CPU列表
     * @param condition
     * @return
     */
    public List<SCMSMonitorCpu> getSCMSMonitorCpuListByCondition(Map<String,Object> condition){
        return scmsMonitorCpuDao.getSCMSMonitorCpuListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public SCMSMonitorCpu getSCMSMonitorCpuById(String id){
        return scmsMonitorCpuDao.getSCMSMonitorCpuById(id);
    }

    /**
     * 添加
     * @param scmsMonitorCpu
     * @return
     */
    public int addSCMSMonitorCpu(SCMSMonitorCpu scmsMonitorCpu){
        return scmsMonitorCpuDao.addSCMSMonitorCpu(scmsMonitorCpu);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delSCMSMonitorCpu(Map<String,Object> condition){
        return scmsMonitorCpuDao.delSCMSMonitorCpu(condition);
    }

    /**
     * 查询监控CPU列表
     * @return
     */
    public List<SCMSMonitorCpu> getSCMSMonitorCpuList(SCMSMonitorCpu scmsMonitorCpu){
        return scmsMonitorCpuDao.getSCMSMonitorCpuList(scmsMonitorCpu);
    }
}
