package jehc.cloud.awstats.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.awstats.model.AuthLog;
import jehc.cloud.awstats.service.AuthLogService;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 授权耗时日志传输对象
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/omp/auth/log")
@Api(value = "授权耗时日志",tags = "授权耗时日志",description = "授权耗时日志")
public class AuthLogController  extends BaseAction{
    @Autowired
    AuthLogService authLogService;

    /**
     * 查询授权耗时日志列表并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询授权耗时日志列表并分页", notes="查询授权耗时日志列表并分页")
    public BasePage getAuthLogListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<AuthLog> authLogList = authLogService.getAuthLogListByCondition(condition);
        PageInfo<AuthLog> page = new PageInfo<AuthLog>(authLogList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个授权耗时日志
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个授权耗时日志", notes="查询单个授权耗时日志")
    public BaseResult<AuthLog> getAuthLogById(@PathVariable("id")String id){
        AuthLog authLog = authLogService.getAuthLogById(id);
        return outDataStr(authLog);
    }

    /**
     * 创建授权耗时日志
     * @param authLogs
     */
    @PostMapping(value="/escalation")
    @AuthUneedLogin
    @ApiOperation(value="创建授权耗时日志", notes="创建授权耗时日志")
    public BaseResult escalation(@RequestBody List<AuthLog> authLogs){
        int i = 0;
        if(null != authLogs){
            for(AuthLog authLog:authLogs){
                authLog.setCreate_time(getDate());
                authLog.setId(toUUID());
            }
            i=authLogService.addAuthLogBatch(authLogs);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 删除授权耗时日志
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除授权耗时日志", notes="删除授权耗时日志")
    public BaseResult delAuthLog(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=authLogService.delAuthLog(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
