package jehc.cloud.awstats.service.impl;

import jehc.cloud.awstats.dao.AuthLogDao;
import jehc.cloud.awstats.model.AuthLog;
import jehc.cloud.awstats.service.AuthLogService;
import jehc.cloud.common.base.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Desc 授权耗时日志传输对象
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class AuthLogServiceImpl extends BaseService implements AuthLogService {

   @Resource
   AuthLogDao authLogDao;

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    public List<AuthLog> getAuthLogListByCondition(Map<String, Object> condition){
        return authLogDao.getAuthLogListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public AuthLog getAuthLogById(String id){
        return authLogDao.getAuthLogById(id);
    }

    /**
     * 批量添加
     * @param authLogs
     * @return
     */
    public int addAuthLogBatch(List<AuthLog> authLogs){
        return authLogDao.addAuthLogBatch(authLogs);
    }


    /**
     * 删除
     * @param condition
     * @return
     */
    public int delAuthLog(Map<String, Object> condition){
        return authLogDao.delAuthLog(condition);
    }
}
