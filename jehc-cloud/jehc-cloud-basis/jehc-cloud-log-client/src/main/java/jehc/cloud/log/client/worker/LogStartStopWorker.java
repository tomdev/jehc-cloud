package jehc.cloud.log.client.worker;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.log.client.entity.LogStartStopDTO;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 服务器启动关闭日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogStartStopWorker implements Runnable{
    LogStartStopDTO logStartStopDTO;//日志信息

    HttpServletRequest request;//请求

    RestTemplateUtil restTemplateUtil;//restTemplateUtil工具类


    public LogStartStopWorker(){

    }

    /**
     *
     * @param logStartStopDTO
     * @param request
     * @param restTemplateUtil
     */
    public LogStartStopWorker(LogStartStopDTO logStartStopDTO, HttpServletRequest request, RestTemplateUtil restTemplateUtil){
        this.logStartStopDTO = logStartStopDTO;
        this.request = request;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录异常日志失败:"+logStartStopDTO+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录启动关闭日志失败开始");
            restTemplateUtil.post(restTemplateUtil.restLogUrl()+"/logStartStop/add", BaseResult.class,logStartStopDTO);
            log.info("记录启动关闭日志失败结束");
        } catch (Exception e) {
            log.info("记录启动关闭日志失败失败:"+logStartStopDTO+"，异常信息：{}",e);
        }
    }
}
