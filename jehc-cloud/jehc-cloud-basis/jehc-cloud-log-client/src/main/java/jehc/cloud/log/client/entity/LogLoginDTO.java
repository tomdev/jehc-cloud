package jehc.cloud.log.client.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* @Desc 登录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:21:01
*/
@Data
public class LogLoginDTO extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String ip;/**登录ip**/
	private String content;/**内容**/
	private String browser_type;/**浏览器类型**/
	private String browser_name;/**浏览器名称**/
	private String browser_version;/**浏览器版本**/
	private String system;/**操作系统**/
	private String create_id;/**创建人id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	private String update_id;/**修改人id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private Integer del_flag;/**删除标记：0正常1已删除**/
}
