package jehc.cloud.log.client.worker;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.log.client.entity.LogModifyRecordDTO;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 记录变更工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogModifyRecordWorker implements Runnable {

    private List<LogModifyRecordDTO> logModifyRecordDTOS;

    private HttpServletRequest request;

    RestTemplateUtil restTemplateUtil;//restTemplateUtil工具类

    /**
     * 无参构造函数
     */
    public LogModifyRecordWorker(){
    }

    /**
     * 有参构造函数
     * @param logModifyRecordDTOS
     */
    public LogModifyRecordWorker(List<LogModifyRecordDTO> logModifyRecordDTOS){
        this.logModifyRecordDTOS = logModifyRecordDTOS;
    }

    /**
     * 有参构造函数
     * @param logModifyRecordDTOS
     * @param request
     * @param restTemplateUtil
     */
    public LogModifyRecordWorker(List<LogModifyRecordDTO> logModifyRecordDTOS, HttpServletRequest request, RestTemplateUtil restTemplateUtil){
        this.logModifyRecordDTOS = logModifyRecordDTOS;
        this.request = request;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录变更记录失败:"+logModifyRecordDTOS.toString()+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录变更记录开始");
            for(LogModifyRecordDTO logModifyRecordDTO: logModifyRecordDTOS){
                restTemplateUtil.post(restTemplateUtil.restLogUrl()+"/logModifyRecord/add", BaseResult.class,logModifyRecordDTO);
            }
            log.info("记录变更记录结束");
        } catch (Exception e) {
            log.info("记录变更记录失败:"+logModifyRecordDTOS.toString()+"，异常信息：{}",e);
        }
    }
}
