package jehc.cloud.common.entity;
import lombok.Data;

/**
 * @Desc 定时任务实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ScheduleJob {
	private String jobId;/**任务id*/
	private String jobTitle;//标题
	private String jobName;/**任务名称*/
	private String jobGroup;/**任务分组*/
	private String jobStatus;/**任务状态 0禁用 1启用 2删除*/
	private String cronExpression;/**任务运行时间表达式*/
	private String desc;/**任务描述*/
	private String clientId;/**执行的类方法*/
	private String clientGroupId;/**执行的类**/
	private String jobHandler;/**注解事件**/
	private String jobPara;/**参数**/

	@Override
	public String toString() {
		return "ScheduleJob{" +
				"jobId='" + jobId + '\'' +
				", jobTitle='" + jobTitle + '\'' +
				", jobName='" + jobName + '\'' +
				", jobGroup='" + jobGroup + '\'' +
				", jobStatus='" + jobStatus + '\'' +
				", cronExpression='" + cronExpression + '\'' +
				", desc='" + desc + '\'' +
				", clientId='" + clientId + '\'' +
				", clientGroupId='" + clientGroupId + '\'' +
				", jobHandler='" + jobHandler + '\'' +
				", jobPara='" + jobPara + '\'' +
				'}';
	}
}