package jehc.cloud.common.util;

import jehc.cloud.common.base.BaseUtils;
import jehc.cloud.common.entity.ModifyRecordEntity;
import jehc.cloud.common.idgeneration.UUID;
import jehc.cloud.common.util.date.DateUtil;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Desc 实体比较
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class RecordEvent {
    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 数据修改对比统计
     * @param oldT
     * @param newT
     * @param className
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> String callBackUpdateInfo(T oldT, T newT, String className) throws Exception{
        Class onwClass = Class.forName(className);
        StringBuffer updateInfo = new StringBuffer();
        Field[] fields = onwClass.getDeclaredFields();
        for (Field f : fields) {
            //过滤 static、 final、private static final字段
            if (f.getModifiers() == 16 || f.getModifiers() == 8
                    || f.getModifiers() == 26) {
                continue;
            }
            Object oldV = ReflectUtil.getFieldValue(oldT,f.getName());
            Object newV = ReflectUtil.getFieldValue(newT,f.getName());
            if (newV != null && !newV.equals(oldV) && StringUtils.isNotBlank(newV.toString())) {
                updateInfo.append(f.getName() + " 从 " +oldV + "修改成" + newV + "<br>");
            }
        }
        return updateInfo.toString();
    }

    /**
     * 数据修改对比统计
     * @param oldT
     * @param newT
     * @param c
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> List callBackListUpdateInfo(T oldT, T newT, Class<?> c) throws Exception{
        List<ModifyRecordEntity> list = new ArrayList<ModifyRecordEntity>();
        Class onwClass = c.getClass();
        Field[] fields = onwClass.getDeclaredFields();
        for (Field f : fields) {
            //过滤 static、 final、private static final字段
            if (f.getModifiers() == 16 || f.getModifiers() == 8
                    || f.getModifiers() == 26) {
                continue;
            }
            /**
             AnnotationColumn annotationColumn = f.getAnnotation(AnnotationColumn.class);
             if (annotationColumn == null) {
             continue;
             }
             **/
            f.setAccessible(true);
            Object oldV = ReflectUtil.getFieldValue(oldT,f.getName());
            Object newV = ReflectUtil.getFieldValue(newT,f.getName());
            String oValue = "";
            if(null != oldV){
                oValue = oValue.toString();
            }
            String nValue = "";
            if(null != newV){
                nValue = newV.toString();
            }
            if (!nValue.equals(oValue)) {
                ModifyRecordEntity record = new ModifyRecordEntity();
                record.setXt_modify_record_aftervalue(""+newV);
                record.setXt_modify_record_beforevalue(""+oldV);
                record.setXt_modify_record_ctime(DateUtil.getSimpleDateFormat());
                record.setXt_modify_record_field(f.getName());
                record.setXt_modify_record_modules(onwClass.getName());
                list.add(record);
            }
        }
        return list;
    }
    /**
     * 执行变更记录
     * @param oldT
     * @param newT
     * @param modules
     * @param business_id
     * @param <T>
     */
    public static <T> void aRecord(T oldT, T newT, String modules,String business_id){
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            JSONObject oldJson = JsonUtil.toJsonObj(oldT);
            JSONObject newJson = JsonUtil.toJsonObj(newT);
            List<ModifyRecordEntity> list = new ArrayList<ModifyRecordEntity>();
            Iterator iterator = oldJson.keys();
            while(iterator.hasNext()){
                String key = (String) iterator.next();
                String oldV = oldJson.getString(key);
                String newV = newJson.getString(key);
                if(!oldV.equals(newV)){
                    ModifyRecordEntity record = new ModifyRecordEntity();
                    record.setXt_modify_record_aftervalue(""+newV);
                    record.setXt_modify_record_beforevalue(""+oldV);
                    record.setXt_modify_record_ctime(DateUtil.getSimpleDateFormat());
                    record.setXt_modify_record_field(key);
                    record.setXt_modify_record_modules(modules);
                    list.add(record);
                }
            }
            for(int i = 0; i < list.size(); i++){
                list.get(i).setXt_modify_record_id(UUID.toUUID());
                list.get(i).setBusiness_id(business_id);
                BaseUtils baseUtils = new BaseUtils();
                list.get(i).setXt_userinfo_id(baseUtils.getXtUid());
            }
        } catch (Exception e) {
        }
    }

    /**
     * 执行变更记录并过滤字段
     * @param oldT
     * @param newT
     * @param modules
     * @param business_id
     * @param fieldList
     * @param <T>
     */
    public static <T> void aRecord(T oldT, T newT, String modules,String business_id,List<String> fieldList){
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            JSONObject oldJson = JsonUtil.toJsonObj(oldT);
            JSONObject newJson = JsonUtil.toJsonObj(newT);
            List<ModifyRecordEntity> list = new ArrayList<ModifyRecordEntity>();
            Iterator iterator = oldJson.keys();
            while(iterator.hasNext()){
                String key = (String) iterator.next();
                if(!fieldList.isEmpty() && fieldList.size() > 0){
                    for(String field:fieldList){
                        if(field.equals(key)){
                            String oldV = oldJson.getString(key);
                            String newV = newJson.getString(key);
                            if(!oldV.equals(newV)){
                                ModifyRecordEntity record = new ModifyRecordEntity();
                                record.setXt_modify_record_aftervalue(""+newV);
                                record.setXt_modify_record_beforevalue(""+oldV);
                                record.setXt_modify_record_ctime(DateUtil.getSimpleDateFormat());
                                record.setXt_modify_record_field(key);
                                record.setXt_modify_record_modules(modules);
                                list.add(record);
                            }
                        }
                    }
                }
            }
            for(int i = 0; i < list.size(); i++){
                list.get(i).setXt_modify_record_id(UUID.toUUID());
                list.get(i).setBusiness_id(business_id);
                BaseUtils baseUtils = new BaseUtils();
                list.get(i).setXt_userinfo_id(baseUtils.getXtUid());
            }
        } catch (Exception e) {
        }
    }
}
