package jehc.cloud.common.entity;

import org.apache.commons.lang3.StringUtils;

/**
 * @Desc 登录实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class LoginEntity {
	private String validateCode;
	private String account;
	private String password;
	private String uid;
	private String baseHttpSessionEntity;
	public String getValidateCode() {
		if(!StringUtils.isEmpty(validateCode)){
			validateCode = validateCode.trim().toLowerCase();;
		}
		return validateCode;
	}
	
	public String getAccount() {
		if(!StringUtils.isEmpty(account)){
			account = account.trim();
		}
		return account;
	}
	public String getPassword() {
		return password;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getBaseHttpSessionEntity() {
		return baseHttpSessionEntity;
	}

	public void setBaseHttpSessionEntity(String baseHttpSessionEntity) {
		this.baseHttpSessionEntity = baseHttpSessionEntity;
	}
}
