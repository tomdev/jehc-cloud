package jehc.cloud.common.constant;
/**
 * @Desc TOPIC常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class TopicConstants {
    public final static String[] SUBTOPICS = new String[]{
            "CHAT","CHAT/#"
    };
}
