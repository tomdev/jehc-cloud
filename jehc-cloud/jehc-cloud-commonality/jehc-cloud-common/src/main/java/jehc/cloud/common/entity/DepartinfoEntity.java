package jehc.cloud.common.entity;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 部门信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class DepartinfoEntity extends BaseEntity {
    private String xt_departinfo_id;/**序列号**/
    private String xt_company_id;/**公司ID**/
    private String xt_departinfo_parentId;/**部门父ID**/
    private String xt_departinfo_name;/**部门名称**/
    private String xt_departinfo_connectTelNo;/**联系电话**/
    private String xt_departinfo_mobileTelNo;/**移动电话**/
    private String xt_departinfo_faxes;/**传真**/
    private String xt_departinfo_desc;/**描述部门信息**/
    private String xt_departinfo_image;/**图片**/
    private int xt_departinfo_leaf;/**是否存在子叶0表示存在子节**/
    private String xt_departinfo_time;/**立成时间**/
    private String xt_departinfo_type;/**部门性质**/
    private String xt_departinfo_parentName;//父名称
}
