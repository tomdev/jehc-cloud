package jehc.cloud.common.handler;
import jehc.cloud.common.annotation.NeedHttpDecrypt;
import jehc.cloud.common.request.RequestWrapper;
import jehc.cloud.common.idgeneration.SnowflakeIdWorker;
import jehc.cloud.common.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @Desc 拦截器传输参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class SignHandler extends HandlerInterceptorAdapter {

    @Resource
    RestTemplateUtil restTemplateUtil;

    @Resource
    SnowflakeIdWorker snowflakeIdWorker;

    @Value("${jehc.sign.type:BODY}")
    private String signType;
    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

        // 1、获取请求的参数
        Map<String, String> params = null;

        HandlerMethod methodHandler =(HandlerMethod) handler;

        Boolean needHttpBodyDecrypt = methodHandler.hasMethodAnnotation(NeedHttpDecrypt.class);//需要解密

        if(needHttpBodyDecrypt){//如果需要解密

            String bodyParameterBody = getBodyParameterBody(request);

            Map<String, String[]> parameterMap = request.getParameterMap();//form-data 参数集合

            Map<String, String> pathVars = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);//Path作为参数获取
            try {
                log.info("Body体参数---{},Form体----{},路径---{}",bodyParameterBody,JsonUtil.toFastJson(parameterMap),pathVars);
//                String json = null;
//                if(this.signType == Constants.SIGN_ALL){//全参数验签
//                    if(!CollectionUtil.isEmpty(parameterMap)){
//                        params.putAll(pathVars);//将Path参数传入进去
//                    }
//                    json=JsonUtil.toFastJson(params);
//                    json+=bodyParameterBody;
//                }else if(this.signType.equals(Constants.SIGN_BODY)){//body验签
//                    json = bodyParameterBody;
//                }else if(this.signType == Constants.SIGN_PATH){//path验签
//                    json=JsonUtil.toFastJson(pathVars);
//                }else if(this.signType == Constants.SIGN_FORM){//form验签
//                    if(!CollectionUtil.isEmpty(parameterMap)){
//                        json=JsonUtil.toFastJson(parameterMap);
//                    }
//                }else{
//
//                }
//                if(!StringUtil.isEmpty(json)){
//                    BaseResult baseResult = DecryptUtils.decrypt(json);
//                    log.info("验签结果: {}, 请求地址: {}, 方法: {}, 参数: {}", baseResult.getData(), request.getRequestURI(), method, params);
//                    if(baseResult.getSuccess()){
//                        return true;
//                    }
//                }
            }catch (Exception e){
                log.warn("验签失败：{}.",e);
                return false;
            }
        }
        return true;
    }

    /**
     * 获取请求入参
     * @param request
     * @return
     */
    public static String getBodyParameterBody(HttpServletRequest request) {
        if(request instanceof RequestWrapper){
            RequestWrapper requestWrapper = (RequestWrapper) request;
            return requestWrapper.getRequestBody();
        }
        return null;
    }
}
